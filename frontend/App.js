import { StatusBar } from 'expo-status-bar';
import { SafeAreaView, Text, View } from 'react-native';
import { TailwindProvider } from 'tailwindcss-react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NewTest from './screens/NewTest';
import Test from './screens/Test';
// import HomeScreen from './components/HomeScreen';
import HomeScreen from './screens/HomeScreen';
import RouteHomePage from './screens/RouteHomePage';
// import RouteCustomize from './screens/RouteCustomize';
// import RouteGenarate from './screens/RouteGenarate';
// import RouteDisplay from './screens/RouteDisplay';
import MarkPlaces from './screens/MarkPlaces'
import Newfinal from './screens/Newfinal';
import Createaccount from './screens/Createaccount';
import Login from './screens/Login';
import Profilepicture from './screens/Profilepicture'
import MarkPlac from './screens/MarkPlac';
import MainScreen from './screens/MainScreen'
import Usersdetails from './screens/Usersdetails'
import EditAccount from './screens/EditAccount'
import LocationHomePage from './screens/LocationHomePage'
import LocationDisplay from './screens/LocationDisplay'
import CustomizeRoute from './screens/CustomizeRoute'
import Routecommonscreen from './screens/Routecommonscreen'
import HotelHome from './screens/HotelHome'
import HotelDisplay from './screens/HotelDisplay'
import HotelTotalDisplay from './screens/HotelTotalDisplay'
import HotelTotalHome from './screens/HotelTotalHome'
import LocationDetails from './screens/LocationDetails'
import HotelWelcomeScreen from './screens/HotelWelcomeScreen'
import HotelProfile from './screens/HotelProfile'


const Stack = createNativeStackNavigator();

export default function App() {
  return (
   <TailwindProvider>
      <NavigationContainer>
          <Stack.Navigator>
          {/* <Stack.Screen name="Home" component={HomeScreen} /> */}
          {/* <Stack.Screen name="RouteHomePage" component={RouteHomePage} /> */}
          {/* <Stack.Screen name="RouteCustomize" component={RouteCustomize} />
          <Stack.Screen name="RouteGenarate" component={RouteGenarate} />
          <Stack.Screen name="RouteDisplay" component={RouteDisplay} />  */}
          
          {/* <Stack.Screen name="HomeScreen" component={HomeScreen} /> */}
          {/* <Stack.Screen name="Test" component={Test} /> */}
          {/* <Stack.Screen name="NewTest" component={NewTest} /> */}
          {/* <Stack.Screen name='Newfinal' component={Newfinal}/> */}
          {/* <Stack.Screen name="MarkPlac" component={MarkPlac} /> */}
          {/* <Stack.Screen name="MarkPlaces" component={MarkPlaces} /> */}
          <Stack.Screen name="Login" component={Login} /> 
          <Stack.Screen name="Createaccount" component={Createaccount} />
          <Stack.Screen name="HomeScreen" component={HomeScreen} />
          <Stack.Screen name="Profilepicture" component={Profilepicture} />
          <Stack.Screen name="RouteHomePage" component={RouteHomePage} />
          <Stack.Screen name="MainScreen" component={MainScreen} />
          <Stack.Screen name="MarkPlac" component={MarkPlac} />
          <Stack.Screen name="NewTest" component={NewTest} />
          <Stack.Screen name="Usersdetails" component={Usersdetails}/>
          <Stack.Screen name="EditAccount" component={EditAccount}/>
          <Stack.Screen name="LocationHomePage" component={LocationHomePage}/>
          <Stack.Screen name="LocationDisplay" component={LocationDisplay}/>
          <Stack.Screen name="CustomizeRoute" component={CustomizeRoute}/>
          <Stack.Screen name="Routecommonscreen" component={Routecommonscreen}/>
          <Stack.Screen name="HotelHome" component={HotelHome}/>
          <Stack.Screen name="HotelDisplay" component={HotelDisplay}/>
          <Stack.Screen name="HotelTotalHome" component={HotelTotalHome}/>
          <Stack.Screen name="HotelTotalDisplay" component={HotelTotalDisplay}/>
          <Stack.Screen name="LocationDetails" component={LocationDetails}/>
          <Stack.Screen name="HotelWelcomeScreen" component={HotelWelcomeScreen}/>
          <Stack.Screen name="HotelProfile" component={HotelProfile}/>

      </Stack.Navigator>
    </NavigationContainer>
   </TailwindProvider>
  );
}


