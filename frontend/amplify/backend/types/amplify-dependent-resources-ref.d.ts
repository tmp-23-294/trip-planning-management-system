export type AmplifyDependentResourcesAttributes = {
  "auth": {
    "frontende5d07c27": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  },
  "storage": {
    "s362c86fca": {
      "BucketName": "string",
      "Region": "string"
    }
  }
}