import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const Header = () => {
  return (
    <View style={styles.header}>
      {/* Gradient background */}
      <View style={styles.gradientBackground}>
        {/* Your project logo */}
        <Image
          source={require('./path/to/your/logo.png')}
          style={styles.logo}
        />
        {/* Project name */}
        <Text style={styles.title}>Trip Planner</Text>
      </View>
      {/* Navigation elements */}
      <View style={styles.navigation}>
        <TouchableOpacity>
          <Ionicons name="ios-search" size={28} color="white" />
        </TouchableOpacity>
        <TouchableOpacity>
          <Ionicons name="ios-person" size={28} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent', // Set a transparent background
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  gradientBackground: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    // You can use a gradient background library for the gradient effect
    // Example: 'linear-gradient(45deg, #FF6B6B, #6B6BFF)'
  },
  logo: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 24,
    color: 'white',
    marginLeft: 10,
    fontWeight: 'bold',
    fontFamily: 'your-custom-font', // Use a custom font if desired
  },
  navigation: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default Header;
