import {
    View,
    Text,
    SafeAreaView,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
  } from "react-native";
  import React, { useEffect, useLayoutEffect, useState } from "react";
  import * as Animatable from "react-native-animatable";
  import { useNavigation } from "@react-navigation/native";
  import { Avatar } from "../assets";
  import { location } from "../assets";
  import { RouteIm } from "../assets";
  import Icon from 'react-native-vector-icons/FontAwesome';

const HomeScreen = () => {
  const navigation = useNavigation();
  const [inputFields, setInputFields] = useState(1);

    useLayoutEffect(() => {
      navigation.setOptions({
        headerShown: false,
      });
    }, []);

   
  return (
    <SafeAreaView className="flex-1 bg-emerald-100 relative">
      <View className="flex-row items-center justify-between px-8">
        <View>
          <Text className="text-[40px] text-[#0B646B] font-bold">Discover</Text>
          <Text className="text-[#527283] text-[36px]">the beauty today</Text>
        </View>

        <View className="w-12 h-12 bg-gray-400 rounded-md items-center justify-center shadow-lg">
          <Image
            source={Avatar}
            className="w-full h-full rounded-md object-cover"
          />
        </View>
      </View>
    </SafeAreaView>
   
  );
};

export default HomeScreen;