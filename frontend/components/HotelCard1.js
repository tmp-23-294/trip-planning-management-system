import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';


const HotelCard1 = ({ hotel }) => {
  const navigation = useNavigation();

  const goToHotelProfile = () => {
    navigation.navigate('HotelProfile', { hotel });
  };

  const hotelImages = {
    "Blue Beach Galle": 'https://i.postimg.cc/NjCVMQP3/185017229.jpg',
    "Villa Upper Dickson": 'https://i.postimg.cc/nrdgNmgP/Villa-Upper-Dickson.jpg', 
    "Villa White Queen": 'https://i.postimg.cc/j5qYwM49/Villa-White-Queen.jpg',
    "Taru Villas Rampart Street": 'https://i.postimg.cc/Prsw877N/Taru-Villas-Rampart-Street.jpg',
    "Mango House": 'https://i.postimg.cc/fLqVL29W/Mango-House.jpg',
    "Yara Galle Fort": 'https://i.postimg.cc/3JLXP1RB/Yara-Galle-Fort.jpg',
    "The Bartizan Galle Fort": 'https://i.postimg.cc/qvTrHs5N/The-Bartizan-Galle-Fort.jpg',
    "Arches Fort": 'https://i.postimg.cc/0Q3ZRCZF/Arches-Fort.jpg',
    "Taavetti": 'https://i.postimg.cc/kXFTpW3M/taavetti.jpg',
    // Add more hotels and their image URLs as needed
  };

  // Get the image URI based on the hotel_name, or use a default image
  const imageUri = hotelImages[hotel.hotel_name] || 'https://default-image-url.com/default-image.jpg'; // Replace with a default image URL



  return (
    <TouchableOpacity onPress={goToHotelProfile} style={styles.card}>
    <View style={styles.container}>
    <View style={styles.imageContainer}>
          <Image source={{ uri: imageUri }} style={styles.hotelImage} />
        </View>
      <View >
        <Text style={styles.hotelName}>{hotel.hotel_name}</Text>
        <Text style={styles.price}>Price: Rs. {hotel.price_per_night}</Text>
      </View>
    </View>
  </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    marginTop:20,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    padding: 16,
    marginVertical: 8,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 3,
    width: Dimensions.get("window").width * 0.9,
    
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageContainer: {
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  hotelImage: {
    width: 80,
    height: 80,
    resizeMode: 'cover',
    borderRadius: 8,
    marginLeft:-15
    
  },

  hotelName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  price: {
    marginTop: 8,
    fontWeight: 'bold',
    fontSize: 13
  },
});


export default HotelCard1;