import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { useNavigation } from "@react-navigation/native";

const navigationBar = ({ isUsersDetailsActive, isProfileDetailsActive }) => {
    const navigation = useNavigation();

    const handleHomePress = () => {
        navigation.navigate("MainScreen");
        // Handle the Home icon press
      };
      const handleUserPress = () => {
        navigation.navigate("Usersdetails");
        // Handle the User icon press
      };
      
      const handleCalendarPress = () => {
       
        // Handle the Calendar icon press
      };
      const handleLocationPress = () => {
        // Handle the Location icon press
      };
      

  return (
    <View style={styles.card}>
      <View style={styles.iconContainer}>
      <TouchableOpacity onPress={handleHomePress}>
      <Icon name="home" type="font-awesome" size={30} color={isUsersDetailsActive ? '#008ae6' : '#000000'} style={styles.inputIcon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={handleUserPress}>
      <Icon name="user" type="font-awesome" size={30} color={isProfileDetailsActive ? '#008ae6' : '#000000'} style={styles.inputIcon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={handleCalendarPress}>
      <Icon name="calendar" type="font-awesome" size={30} color="#000000" style={styles.inputIcon} />
      </TouchableOpacity>
      <TouchableOpacity onPress={handleLocationPress}>
      <Icon name="location-arrow" type="font-awesome" size={30} color="#000000" style={styles.inputIcon} />
      </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#cce0ff',
    padding: 15,
    margin: 0,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: Platform.OS === 'android' ? 6 : 2, // Higher elevation for Android
    width: Dimensions.get("window").width * 1,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  content: {
    fontSize: 16,
  },
  inputIcon: {
    width: 30,
    marginRight: 10,
    marginLeft: 10
  },
  iconContainer: {
    flexDirection: 'row', // Display icons horizontally
     justifyContent: 'space-between', // Distribute icons evenly within the container
    alignItems: 'center',
  },
});

export default navigationBar;
