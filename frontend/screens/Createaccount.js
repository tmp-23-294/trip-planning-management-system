import React, { useState, useLayoutEffect } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { TextInput, Button, RadioButton, Text } from 'react-native-paper';
import { Picker } from '@react-native-picker/picker';
import axios from 'axios';
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage';


const countries = [
  { id: 1, name: 'United States', code: '+1' },
  { id: 2, name: 'Canada', code: '+1' },
  { id: 3, name: 'Sri Lanka', code: '+94'}
  // Add more countries as needed
];

const Createaccount = () => {
  const navigation = useNavigation();
  const [username, setUsername] = useState('');
  const [firstname, setFirstName] = useState('');
  const [lastname, setLastName] = useState('');
  const [gender, setGender] = useState('male');
  const [age, setAge] = useState('');
  const [email, setEmail] = useState('');
  const [telephone, setTelephone] = useState('');
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [password, setPassword] = useState('');

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const handleCountrySelect = (itemValue) => {
    setSelectedCountry(itemValue);
    if (itemValue) {
      setTelephone(itemValue.code + ' ');
    } else {
      setTelephone('');
    }
  };

  const handleSubmit = (e) => {
    // You can handle form submission here, e.g., send the data to a server or perform client-side validation
    console.log({
      username,
      firstname,
      lastname,
      gender,
      age,
      email,
      telephone,
      country: selectedCountry ? selectedCountry.name : '',
      password,
    });

    e.preventDefault();
    const userDetails = {
 

      "fristname" : firstname,
      "username" : username,
      "lastname" : lastname,
      "gender" : gender,
      "age" : age,
      "email" : email,
      "telephone" : telephone,
      "country" : selectedCountry ? selectedCountry.name : '',
      "password" : password
    }
    console.log('userDetails', userDetails);

    axios.post("http://16.171.19.240:5000/users", userDetails).then((res)=>{
      
      AsyncStorage.setItem("username", username);
      console.log(res.data);
     
      console.log(userDetails);
      alert("login success");
      console.log("hello");
      navigation.navigate('Profilepicture');
      Profilepicture

    }).catch((err) =>{

   console.log(err.response.data);
    alert("Invalid accout create");
  })

  };

  return (
    <View style={styles.container}>
      <Text style={{marginTop:20, fontSize:30, alignSelf:'center', marginBottom:10, fontWeight: 'bold',}}>Create Account</Text>
      <TextInput
        label="Username"
        value={username}
        onChangeText={(text) => setUsername(text)}
      />
      <TextInput
        label="First Name"
        value={firstname}
        onChangeText={(text) => setFirstName(text)}
      />
      <TextInput
        label="Last Name"
        value={lastname}
        onChangeText={(text) => setLastName(text)}
      />
      <RadioButton.Group
        onValueChange={(value) => setGender(value)}
        value={gender}
      >
        <RadioButton.Item label="Male" value="male" />
        <RadioButton.Item label="Female" value="female" />
        <RadioButton.Item label="Other" value="other" />
      </RadioButton.Group>
      <TextInput
        label="Age"
        value={age}
        onChangeText={(text) => setAge(text)}
        keyboardType="numeric"
      />
      <TextInput
        label="Email"
        value={email}
        onChangeText={(text) => setEmail(text)}
        keyboardType="email-address"
      />
      <Picker
        selectedValue={selectedCountry}
        onValueChange={handleCountrySelect}
        style={styles.picker}
      >
        <Picker.Item label="Select Country" value={null} />
        {countries.map((country) => (
          <Picker.Item key={country.id} label={country.name} value={country} />
        ))}
      </Picker>
      <TextInput
        label="Telephone"
        value={telephone}
        onChangeText={(text) => setTelephone(text)}
        keyboardType="phone-pad"
      />
      <TextInput
        label="Password"
        value={password}
        onChangeText={(text) => setPassword(text)}
        secureTextEntry
      />
      <Button mode="contained" onPress={handleSubmit}>
        Create Account
      </Button>
      <View style={{ alignItems: "center", height: 100 }}>
            <Text >
              Already have an account?
            </Text>
            <TouchableOpacity
              style={{ margin: 0 }}
              onPress={() => {
                navigation.navigate('Login');
              }}
            >
              <Text
                style={{ fontSize: 16, margin: 8 }}
              >
                SIGN IN
              </Text>
            </TouchableOpacity>
          </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  picker: {
    marginBottom: 12,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
  },
});

export default Createaccount;





