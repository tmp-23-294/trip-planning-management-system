import React, { useState, useRef, useLayoutEffect, useEffect } from 'react';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import * as Location from 'expo-location';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  GooglePlacesAutocomplete,
} from 'react-native-google-places-autocomplete';
import { useNavigation } from '@react-navigation/native';
import { GOOGLE_API_KEY } from '../environments';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const CustomizeRoute = () => {
  const [autocompleteCount, setAutocompleteCount] = useState(1);
  const autocompleteRefs = useRef([]);
  const [selectedLocations, setSelectedLocations] = useState([]);
  const [currentLocationIndex, setCurrentLocationIndex] = useState(0);
  const [location, setLocation] = useState(null);
  const [mapRegion, setMapRegion] = useState({
    latitude: 6.02662175,
    longitude: 80.21768858,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });
  const navigation = useNavigation();
  const mapViewRef = useRef(null);

  const [currentToLocation1Directions, setCurrentToLocation1Directions] = useState(null);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        console.log('Permission to access location was denied');
        return;
      }

      // Get the device's initial location
      try {
        const initialLocation = await Location.getCurrentPositionAsync({});
        setLocation(initialLocation);
      } catch (error) {
        console.error('Error getting initial location:', error);
      }
    })();
  }, []);

  useEffect(() => {
    const locationUpdateInterval = setInterval(async () => {
      try {
        const updatedLocation = await Location.getCurrentPositionAsync({});
        setLocation(updatedLocation);
      } catch (error) {
        console.error('Error getting location:', error);
      }
    }, 5000); // Update location every 5 seconds

    return () => {
      clearInterval(locationUpdateInterval);
      handleContinue();
    };
  }, []);

  const updateUserLocation = () => {
    if (location) {
      // Get the user's current latitude and longitude
      const latitude = location.coords.latitude;
      const longitude = location.coords.longitude;

      // Set the new region to focus on the user's location
      const newRegion = {
        latitude,
        longitude,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      };

      // Update the map's region to focus on the user's location
      return (
        <Marker coordinate={{ latitude, longitude }}>
          <Image source={require('../assets/avatar.png')} style={{ width: 20, height: 20 }} />
        </Marker>
      );
    }

    return null;
  };
  const updateRoute = async () => {
    if (directionsLocationIndex < selectedLocations.length - 1) {
      setDisplayDirections(true);
      setDirectionsLocationIndex(directionsLocationIndex + 1);
    } else {
      setDisplayDirections(false);
      setDirectionsLocationIndex(0);
    }

    if (directionsLocationIndex === 0 && location) {
      // Calculate and set directions from the current user location to location 1
      const origin = `${location.coords.latitude},${location.coords.longitude}`;
      const destination = `${selectedLocations[0].lat},${selectedLocations[0].lng}`;

      fetch(
        `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${GOOGLE_API_KEY}`
      )
        .then((response) => response.json())
        .then((data) => {
          setCurrentToLocation1Directions(data.routes[0].overview_polyline.points);
        })
        .catch((error) => {
          console.error('Error fetching directions:', error);
        });
    }
  }

  const handleAddAutocomplete = () => {
    setAutocompleteCount(prevCount => prevCount + 1);
  };

  const handleRemoveAutocomplete = index => {
    if (autocompleteCount > 1) {
      setAutocompleteCount(prevCount => prevCount - 1);
      autocompleteRefs.current.splice(index, 1);
      setSelectedLocations(prevLocations =>
        prevLocations.filter((_, i) => i !== index)
      );
    }
  };

  const [displayDirections, setDisplayDirections] = useState(false);
  const [directionsLocationIndex, setDirectionsLocationIndex] = useState(0);

  const handleContinue = () => {
    if (directionsLocationIndex < selectedLocations.length - 1) {
      setDisplayDirections(true);
      setDirectionsLocationIndex(directionsLocationIndex + 1);
    } else {
      setDisplayDirections(false);
      setDirectionsLocationIndex(0);
    }

    if (directionsLocationIndex === 0 && location) {
      // Calculate and set directions from the current user location to location 1
      const origin = `${location.coords.latitude},${location.coords.longitude}`;
      const destination = `${selectedLocations[0].lat},${selectedLocations[0].lng}`;

      fetch(
        `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${GOOGLE_API_KEY}`
      )
        .then((response) => response.json())
        .then((data) => {
          setCurrentToLocation1Directions(data.routes[0].overview_polyline.points);
        })
        .catch((error) => {
          console.error('Error fetching directions:', error);
        });
    }
  };

  const handlePlaceSelect = (index, data, details) => {
    if (details && details.geometry && details.geometry.location) {
      const { lat, lng } = details.geometry.location;
      const newLocation = { lat, lng };
      setSelectedLocations(prevLocations => [...prevLocations, newLocation]);
      setCurrentLocationIndex(index);

      const allLocations = [...selectedLocations, newLocation];
      const currentLocation = {
        lat: mapRegion.latitude,
        lng: mapRegion.longitude,
      };
      allLocations.push(currentLocation);

      let minLat = allLocations[0].lat;
      let maxLat = allLocations[0].lat;
      let minLng = allLocations[0].lng;
      let maxLng = allLocations[0].lng;

      allLocations.forEach(location => {
        minLat = Math.min(minLat, location.lat);
        maxLat = Math.max(maxLat, location.lat);
        minLng = Math.min(minLng, location.lng);
        maxLng = Math.max(maxLng, location.lng);
      });

      const zoomedOutFactor = 1.5;

      const newMapRegion = {
        latitude: (minLat + maxLat) / 2,
        longitude: (minLng + maxLng) / 2,
        latitudeDelta: (maxLat - minLat) * zoomedOutFactor + LATITUDE_DELTA,
        longitudeDelta: (maxLng - minLng) * zoomedOutFactor + LONGITUDE_DELTA,
      };

      setMapRegion(newMapRegion);

      mapViewRef.current.animateToRegion(newMapRegion, 1000);
    }
  };

  return (
    <View style={styles.container}>
      {Array.from({ length: autocompleteCount }).map((_, index) => (
        <View key={index} style={styles.autocompleteContainer}>
          <GooglePlacesAutocomplete
            ref={ref => (autocompleteRefs.current[index] = ref)}
            placeholder={`Search ${index + 1}`}
            fetchDetails
            onPress={(data, details) => handlePlaceSelect(index, data, details)}
            query={{
              key: GOOGLE_API_KEY,
              language: 'en',
            }}
          />
          <TouchableOpacity
            style={styles.minusButton}
            onPress={() => handleRemoveAutocomplete(index)}
          >
            <Text style={styles.minusButtonText}>-</Text>
          </TouchableOpacity>
        </View>
      ))}

      <TouchableOpacity style={styles.plusButton} onPress={handleAddAutocomplete}>
        <Text style={styles.plusButtonText}>+</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.continueButton} onPress={handleContinue}>
        <Text style={styles.continueButtonText}>Continue</Text>
      </TouchableOpacity>

      <MapView
        ref={mapViewRef}
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        region={mapRegion}
        userLocationAnnotationTitle="You are here"
      >
        {selectedLocations.map((location, index) => (
          <Marker
            key={index}
            coordinate={{
              latitude: location.lat,
              longitude: location.lng,
            }}
            title={`Location ${index + 1}`}
          />
        ))}

        {currentToLocation1Directions && (
          <MapViewDirections
            origin={location && `${location.coords.latitude},${location.coords.longitude}`}
            destination={`${selectedLocations[0].lat},${selectedLocations[0].lng}`}
            apikey={GOOGLE_API_KEY}
            strokeWidth={3}
            strokeColor="blue" // You can customize the color
            waypoints={[]}
            polyline={currentToLocation1Directions}
          />
        )}

        {selectedLocations.length >= 2 && displayDirections && (
          selectedLocations.slice(1).map((location, index) => (
            <MapViewDirections
              key={index}
              origin={`${selectedLocations[index].lat},${selectedLocations[index].lng}`}
              destination={`${location.lat},${location.lng}`}
              apikey={GOOGLE_API_KEY}
              strokeWidth={3}
              strokeColor="blue"
            />
          ))
        )}
        {updateUserLocation()}
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    flex: 1,
    padding: 20,
  },
  autocompleteContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  plusButton: {
    backgroundColor: 'blue',
    width: 50,
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
    right: 20,
    zIndex: 1,
  },
  plusButtonText: {
    fontSize: 24,
    color: 'white',
  },
  minusButton: {
    backgroundColor: 'red',
    width: 30,
    height: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
  },
  minusButtonText: {
    fontSize: 20,
    color: 'white',
  },
  continueButton: {
    backgroundColor: 'green',
    width: 100,
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
    left: 20,
    zIndex: 1,
  },
  continueButtonText: {
    fontSize: 18,
    color: 'white',
  },
  map: {
    flex: 1,
  },
});

export default CustomizeRoute;
