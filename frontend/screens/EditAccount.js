import React, { useEffect, useState } from "react";
import { View, StyleSheet, Image, ActivityIndicator, TouchableOpacity, ScrollView } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import { TextInput, Button, RadioButton, Text } from 'react-native-paper';
import { Picker } from '@react-native-picker/picker';

import { Amplify, Storage } from 'aws-amplify';
import awsconfig from '../src/aws-exports';
Amplify.configure(awsconfig);


const countries = [
  { id: 1, name: 'United States', code: '+1' },
  { id: 2, name: 'Canada', code: '+1' },
  { id: 3, name: 'Sri Lanka', code: '+94'}
  // Add more countries as needed
];

const EditAccount = () => {
  const navigation = useNavigation();
  const [s3Image, setS3Image] = useState(null);
  const [userData, setUserData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const [username, setUsername] = useState('');
  const [firstname, setFirstName] = useState('');
  const [lastname, setLastName] = useState('');
  const [gender, setGender] = useState('');
  const [age, setAge] = useState('');
  const [email, setEmail] = useState('');
  const [telephone, setTelephone] = useState('');
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [password, setPassword] = useState('');

  useEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });

    async function getUserData() {
      const username = await AsyncStorage.getItem("username");

      try {
        const userDetailsResponse = await axios.get(
          `http://16.171.19.240:5000/users/${username}`
        );
        setUserData(userDetailsResponse.data.user);
        console.log(userDetailsResponse.data.user);
        setUsername(userDetailsResponse.data.user.username);
        setFirstName(userDetailsResponse.data.user.fristname);
        setLastName(userDetailsResponse.data.user.lastname);
        setGender(userDetailsResponse.data.user.gender);
        setAge(userDetailsResponse.data.user.age);
        setEmail(userDetailsResponse.data.user.email);
        setTelephone(userDetailsResponse.data.user.telephone);
        setSelectedCountry(userDetailsResponse.data.user.country);
        setPassword(userDetailsResponse.data.user.password);
        console.log(userDetailsResponse.data.user.country);
      } catch (error) {
        alert(error.msg);
      }

      try {
        const imageURI = await Storage.get(`profPic/${username}/${username}.jpg`);
        setS3Image(imageURI);
      } catch (error) {
        console.log(error);
      }

      setIsLoading(false);
    }

    getUserData();
  }, []);

  const handleCountrySelect = (itemValue) => {
    setSelectedCountry(itemValue);
    if (itemValue) {
      setTelephone(itemValue.code + ' ');
    } else {
      setTelephone('');
    }
  };


  async function updateUserData() {
    const userDetails = {
 

      "fristname" : firstname,
      "username" : username,
      "lastname" : lastname,
      "gender" : gender,
      "age" : age,
      "email" : email,
      "telephone" : telephone,
      "country" : selectedCountry ? selectedCountry.name : '',
      "password" : password
    }
    console.log('userDetails', userDetails);
    const username = await AsyncStorage.getItem("username");
    axios
  .put(`http://16.171.19.240:5000/users/${username}`, userDetails)
  .then(() => {
      alert("User Updated Successfully!");
      navigation.navigate("MainScreen");
  })
  .catch((err) => {
    alert(err);
  });
  }
  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#333" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView>
    <Text style={{marginTop:20, fontSize:30, alignSelf:'center', marginBottom:10, fontWeight: 'bold',}}>Edit Account</Text>
        
      
        {s3Image && (
          <Image source={{ uri: s3Image }} style={styles.profileImage} />
        )}

        <View >
        {/* <Text style={{marginTop:20, fontSize:30, alignSelf:'center', marginBottom:10, fontWeight: 'bold',}}>Create Account</Text> */}
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Username:</Text>
          <Text style={styles.detailText}>{userData?.username}</Text>
        </View>
      <TextInput
        label="First Name"
        value={firstname}
        onChangeText={(text) => setFirstName(text)}
      />
      <TextInput
        label="Last Name"
        value={lastname}
        onChangeText={(text) => setLastName(text)}
      />
      <RadioButton.Group
        onValueChange={(value) => setGender(value)}
        value={gender}
      >
        <RadioButton.Item label="Male" value="male" />
        <RadioButton.Item label="Female" value="female" />
        <RadioButton.Item label="Other" value="other" />
      </RadioButton.Group>
      <TextInput
        label="Age"
        value={age}
        onChangeText={(text) => setAge(text)}
        keyboardType="numeric"
      />
      <TextInput
        label="Email"
        value={email}
        onChangeText={(text) => setEmail(text)}
        keyboardType="email-address"
      />
      <Picker
      value={selectedCountry}
        selectedValue={selectedCountry}
        onValueChange={handleCountrySelect}
        style={styles.picker}
      >
        <Picker.Item label="Select Country" value={null} />
        {countries.map((country) => (
          <Picker.Item key={country.id} label={country.name} value={country} />
        ))}
      </Picker>
      <TextInput
        label="Telephone"
        value={telephone}
        onChangeText={(text) => setTelephone(text)}
        keyboardType="phone-pad"
      />
      <TextInput
        label="Password"
        value={password}
        onChangeText={(text) => setPassword(text)}
        secureTextEntry
      />
        </View>
        <TouchableOpacity
      style={styles.editButton}
      onPress={() => {
        // Navigate to the Edit Account screen
        updateUserData()
      }}
    >
      <Text style={styles.editButtonText}>Edit Account</Text>
    </TouchableOpacity>
    </ScrollView>
      </View>
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  picker: {
    marginBottom: 12,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 4,
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    width: "90%",
  },
  heading: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#333",
  },
  detailContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
  detailLabel: {
    flex: 1,
    fontWeight: "bold",
    color: "#666",
    fontSize: 18
  },
  detailText: {
    flex: 2,
    color: "#333",
    fontSize: 18
  },
  profileImage: {
    width: 100,
    height: 100,
    resizeMode: "cover",
    borderRadius: 50,
    alignSelf: "center",
    marginTop: 0,
    marginBottom: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  editButton: {
    backgroundColor: "#333",
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignSelf: "center",
    marginTop: 20,
  },
  editButtonText: {
    color: "#fff",
    fontWeight: "bold",
  },
});

export default EditAccount;
