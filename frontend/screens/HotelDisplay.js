import React, { useState, useLayoutEffect, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, TextInput, TouchableOpacity, Dimensions, Image } from 'react-native';
import { useNavigation } from "@react-navigation/native";
import axios from 'axios';
import HotelCard1 from '../components/HotelCard1'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../components/NavigationBar';
import { routehero } from "../assets";

export default function HotelDisplay() {
  const navigation = useNavigation();
  const [hotel, setHotels]=useState([]);
  const [hotelName, setHotelName] = useState('');
  const [city, setCity] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const  handleSearch =  () => {
    // setIsLoading(true);
    // Perform search based on city and maxPrice values
    // For now, just log the values to the console
    console.log('City:', city);
    console.log('Max Price:', maxPrice);
    console.log('Hotel Name:', hotelName);
    const mprice = parseInt(maxPrice);
    const newdata={     
            "hotelCity": city,
            "hotelPrice": mprice,
            "hotelName": hotelName
                }
                console.log('new data:', newdata);
    axios
    .post("http://16.171.35.128:5000/apii",newdata)
    .then((res) => {
        // setIsLoading(false);
        console.log('ABC:');
     console.log(res.data);
     navigation.navigate("HotelTotalDisplay");
    })
    .catch((err) => {
        // setIsLoading(false);
      alert(err.msg);
   });
  };

  useEffect(()=>{
    (async () => {
        function getHotels() {
            axios
              .get("http://16.171.19.240:5000/displayy")
              .then((res) => {
                console.log(res.data.hotelflask.result);
                setHotels(res.data.hotelflask.result);
              })
              .catch((err) => {
                alert(err.msg);
              });
          }
          getHotels();
          const city = await AsyncStorage.getItem('city');
          console.log('Cityyyyy:', city);
          setCity(city);
          const maxPrice = await AsyncStorage.getItem('maxPrice');
          setMaxPrice(maxPrice);
        })();
},[])

  return (
    <View style={styles.container}>
      
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
            <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
            <Text style={styles.welcomeText}>Filter More Hotels Like       Your Previous Choice</Text>
            
            <TextInput
        style={styles.input}
        placeholder="Search More Hotels"
        value={hotelName}
        onChangeText={setHotelName}
      />
        <TouchableOpacity style={styles.button} onPress={handleSearch}>
        <Text style={styles.buttonText}>Search Hotels</Text>
      </TouchableOpacity>
      
          
          </View>
  <View style={styles.whiteCard}>
  <ScrollView>
          {hotel.slice(0, 3).map((hotel, index) => (
            <HotelCard1 key={index} hotel={hotel} style={{width:Dimensions.get("window").width * 0.45}}/>
          ))}
    </ScrollView>
    </View>
    <NavigationBar/>
  </View>
  );
}

const styles = StyleSheet.create({
  
  container: {
    flex: 1,
    marginTop:60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
},

input: {
  width: 230,
  height: 40,
  borderColor: 'gray',
  backgroundColor:'white',
  borderWidth: 1,
  borderRadius: 5,
  marginBottom: 10,
  paddingHorizontal: 10,
  shadowColor: 'black',  // Color of the shadow
  shadowOffset: { width: 0, height: 2 }, // Shadow offset
  shadowOpacity: 0.2,     // Shadow opacity
  shadowRadius: 6,       // Shadow radius
  elevation: 4, 
},
button: {
  backgroundColor: '#4287f5',
  borderRadius: 10,
  padding: 10,
  width: '60%',
  alignItems: 'center',
  shadowColor: 'black',  // Color of the shadow
  shadowOffset: { width: 0, height: 2 }, // Shadow offset
  shadowOpacity: 0.2,     // Shadow opacity
  shadowRadius: 6,       // Shadow radius
  elevation: 4, 
},
buttonText: {
  color: '#fff',
  fontWeight: 'bold',
},

welcomeText: {
  fontSize: 30,
  marginTop: 0,
  fontWeight: 'bold',
  textAlign: 'center',
  marginBottom: 20,
  color: 'white',
},
subText: {
  fontSize: 16,
  textAlign: 'center',
  color: 'white',
},
blueCard: {
  backgroundColor: 'rgb(102, 153, 255)',
  width: '100%', // Take up the entire width
  height: '53%', // Take up half of the screen height
  borderTopLeftRadius: 10,
  borderTopRightRadius: 10,
  alignItems: 'center',
  justifyContent: 'center',
  marginTop:100,
},
whiteCard: {
  backgroundColor: 'white',
  width: '100%', // Take up the entire width
  height: '60%', // Take up half of the screen height
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: '-15%',
  shadowColor: 'black',  // Color of the shadow
  shadowOffset: { width: 0, height: 2 }, // Shadow offset
  shadowOpacity: 0.2,     // Shadow opacity
  shadowRadius: 6,       // Shadow radius
  elevation: 4, 
},
});