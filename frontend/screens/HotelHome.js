import React, { useState, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TextInput,ActivityIndicator, TouchableOpacity, Image, ScrollView, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../components/NavigationBar';
import { routehero } from "../assets";

export default function HotelHome() {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const [city, setCity] = useState('');
  const [maxPrice, setMaxPrice] = useState('');

  const handleSearch = async () => {
    setIsLoading(true);
    // Perform search based on city and maxPrice values
    // For now, just log the values to the console
    console.log('City:', city);
    console.log('Max Price:', maxPrice);
    const mprice = parseInt(maxPrice);
    AsyncStorage.setItem("city", city);
   
    AsyncStorage.setItem("maxPrice", maxPrice);
    const newdata={     
            "hotelCity": city,
            "hotelPrice": mprice   
                }
    axios
    .post("http://16.171.19.240:5000/apiii",newdata)
    .then((res) => {
        setIsLoading(false);
     console.log(res.data);
     navigation.navigate("HotelDisplay");
    })
    .catch((err) => {
        setIsLoading(false);
      alert(err.msg);
   });
  };
  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#333" />
      </View>
    );
  }

  return (
 

    <View style={styles.container}>
      
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
          <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
          <Text style={styles.welcomeText}>Filter More Hotels Like       Your Previous Choice</Text>
          


    
    
        
        </View>
<View style={styles.whiteCard}>
<ScrollView  scrollEnabled={false}>
<View style={{justifyContent:'center', alignItems:'center', marginTop:60, width:300,  width: Dimensions.get("window").width*0.8,}}>
<Text style={styles.subBelowText}>This feature allows you to find the best hotel recommendations based on your preferred city and maximum price. This will provide a tailored recommendations that match your specific criteria.</Text>
<TextInput
        style={styles.input}
        placeholder="Enter City"
        value={city}
        onChangeText={setCity}
      />
      <TextInput
        style={styles.input}
        placeholder="Enter Max Price"
        value={maxPrice}
        onChangeText={setMaxPrice}
        keyboardType="numeric"
      />
      <TouchableOpacity style={styles.button} onPress={handleSearch}>
        <Text style={styles.buttonText}>Search Hotels</Text>
      </TouchableOpacity>
      </View>
  </ScrollView>
  </View>
  <NavigationBar/>
</View>
    );

}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  subBelowText: {
    fontSize: 17,
    textAlign: 'center',
    marginBottom: 40,
    textAlign: 'justify',
    fontWeight:'bold'
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#4287f5',
    borderRadius: 10,
    padding: 10,
    width: '60%',
    alignItems: 'center',
    marginBottom: 210,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    //marginBottom:500
  },
  welcomeText: {
    fontSize: 30,
    marginTop: 75,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  subText: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
  },
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  welcomeCard: {
    backgroundColor: 'rgb(102, 153, 255)', // Change the color here
    padding: 20,
    height: 500,
    marginTop:-360,
    width:500,
    borderBottomRightRadius:60,
    borderBottomLeftRadius: 60,
    // borderBottomRightRadius: 80,
    // borderBottomLeftRadius: 280,
    shadowColor: 'black', 
    shadowOffset: { width: 0, height: 2 }, 
    shadowOpacity: 0.2,     
    shadowRadius: 6,      
    elevation: 4, 
  },

  subText: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
  },
  blueCard: {
    backgroundColor: 'rgb(102, 153, 255)',
    width: '100%', // Take up the entire width
    height: '40%', // Take up half of the screen height
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  whiteCard: {
    backgroundColor: 'white',
    width: '100%', // Take up the entire width
    height: '59%', // Take up half of the screen height
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '-15%',
    shadowColor: 'black',  // Color of the shadow
    shadowOffset: { width: 0, height: 2 }, // Shadow offset
    shadowOpacity: 0.2,     // Shadow opacity
    shadowRadius: 6,       // Shadow radius
    elevation:4,
},
});