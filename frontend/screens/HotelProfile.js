import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Linking, Image, Dimensions, SafeAreaView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons'; // Assuming you're using Expo icons
import NavigationBar from '../components/NavigationBar';
import { ScrollView } from 'react-native';
import { routehero } from "../assets";


export default function HotelProfile({ route }) {
  
  const { hotel } = route.params;
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const redirectToMap = () => {
    const addressForMap = `${hotel.hotel_name}, ${hotel.hotel_address}`;
    // const addressForMap = hotel.hotel_name + ', ' + hotel.hotel_address;
    const mapsUrl = `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(
      addressForMap
    )}`;
    Linking.openURL(mapsUrl);
  };
  const hotelImages = {
    "Blue Beach Galle": 'https://i.postimg.cc/NjCVMQP3/185017229.jpg',
    "Villa Upper Dickson": 'https://i.postimg.cc/nrdgNmgP/Villa-Upper-Dickson.jpg', 
    "Villa White Queen": 'https://i.postimg.cc/j5qYwM49/Villa-White-Queen.jpg',
    "Taru Villas Rampart Street": 'https://i.postimg.cc/Prsw877N/Taru-Villas-Rampart-Street.jpg',
    "Mango House": 'https://i.postimg.cc/fLqVL29W/Mango-House.jpg',
    "Yara Galle Fort": 'https://i.postimg.cc/3JLXP1RB/Yara-Galle-Fort.jpg',
    "The Bartizan Galle Fort": 'https://i.postimg.cc/qvTrHs5N/The-Bartizan-Galle-Fort.jpg',
    "Arches Fort": 'https://i.postimg.cc/0Q3ZRCZF/Arches-Fort.jpg',
    "Taavetti": 'https://i.postimg.cc/kXFTpW3M/taavetti.jpg',
    // Add more hotels and their image URLs as needed
  };

  // Get the image URI based on the hotel_name, or use a default image
  const imageUri = hotelImages[hotel.hotel_name] || 'https://default-image-url.com/default-image.jpg'; // Replace with a default image URL


  const saveHotel = () => {
    // Implement save hotel functionality here
  };

  return (

    
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
    
      
 

    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
            <Image source={{ uri: imageUri }} style={{ width: "100%", height: 405, resizeMode: "cover", position: "absolute", top: 5,  }} />
          </View>
        <View style={styles.container}>
        
        <ScrollView>
<View >
      <View style={styles.card}>
      <Text style={styles.hotelName}>{hotel.hotel_name}</Text>
      <Text style={styles.subText}>Most popular facilities</Text>
      <Text style={styles.description}>{hotel.hotel_description}</Text>
      <Text style={styles.subText2}>Addresss</Text>
      <Text style={styles.address}>{hotel.hotel_address}</Text>
      <Text style={styles.price}>Price: $ {hotel.price_per_night}</Text>
      <View style={styles.buttonContainer}>
  <TouchableOpacity onPress={redirectToMap} style={styles.iconButton}>
    <Ionicons name="md-map" size={24} color="white" />
    <Text style={styles.buttonText}>Redirect to Map</Text>
  </TouchableOpacity>
  <TouchableOpacity onPress={saveHotel} style={styles.iconButton}>
    <Ionicons name="md-flag" size={24} color="white" />
    <Text style={styles.buttonText}>Save Hotel</Text>
  </TouchableOpacity>
          
        </View>
        </View>
      
        </View>
        </ScrollView>
        <NavigationBar/>
    </View>

</SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop:-200
  },
  welcomeText: {
    fontSize: 30,
    marginTop: -145,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  hotelImage: {
    width: 400, // Increase the width for a larger image
    height: 360, // Increase the height for a larger image
    marginTop: 0, // Move the image closer to the top margin
    resizeMode: 'cover',
    borderRadius: 5,

  },
  whiteCard: {
    backgroundColor: 'white',
    width: '90%', // Take up the entire width
    height: '75%', // Take up half of the screen height
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '-15%',
    shadowColor: 'black',  // Color of the shadow
    shadowOffset: { width: 0, height: 2 }, // Shadow offset
    shadowOpacity: 0.2,     // Shadow opacity
    shadowRadius: 6,       // Shadow radius
    elevation: 4, 
  },
  hotelName: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 10, // Add margin to separate the name from the image
  },
  description: {
    fontSize: 16,
    marginLeft: 40,
    marginRight: 40,
    marginTop: 10, // Add margin to separate the description from the name
    textAlign: 'justify',
  },
  address: {
    fontSize: 16,
    marginLeft: 40,
    marginRight: 40,
    marginTop: 10, // Add margin to separate the address from the description
    textAlign: 'center',
  },
  price: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20, // Add margin to separate the price from the address
  },
  card: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    padding: 16,
    marginVertical: 8,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 3,
    width: Dimensions.get("window").width * 1,
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 60,
    borderTopRightRadius: 60
  },
  buttonContainer: {
    flexDirection: 'row', // Arrange the buttons horizontally
    justifyContent: 'space-between', // Add space between buttons
    marginTop:20, // Adjust as needed
  },
  iconButton: {
    flexDirection: 'row', // Arrange the icon and text horizontally
    alignItems: 'center', // Center them vertically
    backgroundColor: '#4287f5',
    borderRadius: 10,
    padding: 10,
    width: '38%', // Adjust the width as needed to fit both buttons
    justifyContent: 'center', // Center the content horizontally
    marginLeft: 10,
    marginRight: 10,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 5, // Add some spacing between icon and text
  },
  subText: {
    fontSize: 16,
    marginBottom: 0,
    marginTop: 20,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    marginLeft:-150,
  },
  subText2: {
    marginTop:20,
    marginLeft:-255,
    fontSize: 16,
    textAlign: 'left',
    color: 'black',
    fontWeight: 'bold',
},
});