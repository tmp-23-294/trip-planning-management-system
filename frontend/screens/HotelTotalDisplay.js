import React, { useState, useLayoutEffect, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, Dimensions} from 'react-native';
import { useNavigation } from "@react-navigation/native";
import axios from 'axios';
import HotelCard from '../components/HotelCard';
import NavigationBar from '../components/NavigationBar';
import { routehero } from "../assets";
import { Icon } from 'react-native-elements';

export default function HotelTotalDisplay() {
  const navigation = useNavigation();
  const [hotel, setHotels]=useState([]);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  useEffect(()=>{
        function getHotels() {
            axios
              .get("http://16.171.35.128:5000/displayhotel")
              .then((res) => {
                console.log(res.data.hotelTotalflask.result);
                setHotels(res.data.hotelTotalflask.result);
              })
              .catch((err) => {
                alert(err.msg);
              });
          }
          getHotels();
     
},[])

  return (
    <View style={styles.container}>
      
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
          <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
          <Text style={styles.welcomeText}>Shore More Hotels</Text>
          <Icon name="university" type="font-awesome" size={60} color="white"/>
                  
        </View>
<View style={styles.whiteCard}>
<ScrollView>
      {hotel.map((hotel, index) => (
        <HotelCard key={index} hotel={hotel} />
      ))}
    </ScrollView>
  </View>
  <NavigationBar/>
</View>
  );
}

const styles = StyleSheet.create({
  
  container: {
    flex: 1,
    marginTop:60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
},

input: {
  width: 230,
  height: 40,
  borderColor: 'gray',
  backgroundColor:'white',
  borderWidth: 1,
  borderRadius: 5,
  marginBottom: 10,
  paddingHorizontal: 10,
  shadowColor: 'black',  // Color of the shadow
  shadowOffset: { width: 0, height: 2 }, // Shadow offset
  shadowOpacity: 0.2,     // Shadow opacity
  shadowRadius: 6,       // Shadow radius
  elevation: 4, 
},
button: {
  backgroundColor: '#4287f5',
  borderRadius: 10,
  padding: 10,
  width: '60%',
  alignItems: 'center',
  shadowColor: 'black',  // Color of the shadow
  shadowOffset: { width: 0, height: 2 }, // Shadow offset
  shadowOpacity: 0.2,     // Shadow opacity
  shadowRadius: 6,       // Shadow radius
  elevation: 4, 
},
buttonText: {
  color: '#fff',
  fontWeight: 'bold',
},

welcomeText: {
  fontSize: 30,
  marginTop: 0,
  fontWeight: 'bold',
  textAlign: 'center',
  marginBottom: 20,
  color: 'white',
},
subText: {
  fontSize: 16,
  textAlign: 'center',
  color: 'white',
},
blueCard: {
  backgroundColor: 'rgb(102, 153, 255)',
  width: '100%', // Take up the entire width
  height: '53%', // Take up half of the screen height
  borderTopLeftRadius: 10,
  borderTopRightRadius: 10,
  alignItems: 'center',
  justifyContent: 'center',
  marginTop:100,
},
whiteCard: {
  backgroundColor: 'white',
  width: '100%', // Take up the entire width
  height: '70%', // Take up half of the screen height
  borderTopLeftRadius: 40,
  borderTopRightRadius: 40,
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: '-15%',
  shadowColor: 'black',  // Color of the shadow
  shadowOffset: { width: 0, height: 2 }, // Shadow offset
  shadowOpacity: 0.2,     // Shadow opacity
  shadowRadius: 6,       // Shadow radius
  elevation: 4, 
},
});