import React, { useState, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TextInput,ActivityIndicator, TouchableOpacity, ImageBackground } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';

export default function HotelTotalHome() {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const [city, setCity] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [hotelName, setHotelName] = useState('');

  const handleSearch = () => {
    setIsLoading(true);
    // Perform search based on city and maxPrice values
    // For now, just log the values to the console
    console.log('City:', city);
    console.log('Max Price:', maxPrice);
    console.log('Hotel Name:', hotelName);
    const mprice = parseInt(maxPrice);
    const newdata={     
            "hotelCity": city,
            "hotelPrice": mprice,
            "hotelName": hotelName
                }
                console.log('new data:', newdata);
    axios
    .post("http://16.171.35.128:5000/apii",newdata)
    .then((res) => {
        setIsLoading(false);
     console.log(res.data);
    //  navigation.navigate("HotelDisplay");
    })
    .catch((err) => {
        setIsLoading(false);
      alert(err.msg);
   });
  };
  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#333" />
      </View>
    );
  }

  return (
    <ImageBackground
    source={{ uri: 'https://i.postimg.cc/K8Gsn53k/videoblocks-lobby-hotel-blurred-background-bn-fsihsf-thumbnail-1080-01.png' }}
    style={styles.imageBackground}
    >
    <View style={styles.container}>
      <Text style={styles.text}>Hotel Search</Text>
      <TextInput
        style={styles.input}
        placeholder="Enter City"
        value={city}
        onChangeText={setCity}
      />
      <TextInput
        style={styles.input}
        placeholder="Enter Max Price"
        value={maxPrice}
        onChangeText={setMaxPrice}
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        placeholder="Enter Hotel Name"
        value={hotelName}
        onChangeText={setHotelName}
      />
      <TouchableOpacity style={styles.button} onPress={handleSearch}>
        <Text style={styles.buttonText}>Search Hotels</Text>
      </TouchableOpacity>
    </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: '#4287f5',
    borderRadius: 10,
    padding: 10,
    width: '60%',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
},
});