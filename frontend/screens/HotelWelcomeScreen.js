import React, { useState, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TextInput,ActivityIndicator, TouchableOpacity, ImageBackground, Image, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../components/NavigationBar';
import { routehero } from "../assets";
import { Icon } from 'react-native-elements';

export default function HotelHome() {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const [city, setCity] = useState('');
  const [maxPrice, setMaxPrice] = useState('');

  const handleSearch = async () => {
    setIsLoading(true);
    // Perform search based on city and maxPrice values
    // For now, just log the values to the console
    console.log('City:', city);
    console.log('Max Price:', maxPrice);
    const mprice = parseInt(maxPrice);
    AsyncStorage.setItem("city", city);
   
    AsyncStorage.setItem("maxPrice", maxPrice);
    const newdata={     
            "hotelCity": city,
            "hotelPrice": mprice   
                }
    axios
    .post("http://16.171.19.240:5000/apiii",newdata)
    .then((res) => {
        setIsLoading(false);
     console.log(res.data);
     navigation.navigate("HotelDisplay");
    })
    .catch((err) => {
        setIsLoading(false);
      alert(err.msg);
    });
  };
  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#333" />
      </View>
    );
  }

  return (
   
    <View style={styles.container}>
        {/* Welcome Message Card */}
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70, }}>
            <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5, borderBottomRightRadius:45,  borderBottomLeftRadius: 60, }} />
          </View>
        <View >
        <Text style={styles.welcomeText}>Welcome to our Hotel Recommendation!</Text>
        <Text style={styles.subText1}>Discover the perfect stay just a tap away.</Text>
        <Text style={styles.subText}>Let's find your ideal hotel!</Text>
        <Icon name="university" type="font-awesome" size={60} color="white"/>
      </View>
    

      <View style={styles.mwhiteCard}>
      <ScrollView >

      <View style={styles.whiteCard}>
          <Text style={styles.subBelowText}>This feature allows you to find the best hotel recommendations based on your preferred city and maximum price. This will provide a tailored recommendations that match your specific criteria.</Text>
          <TouchableOpacity
                style={styles.button}
                onPress={() => navigation.navigate('HotelHome')} // Navigate to HotelHome screen
              >
                <Text style={styles.buttonText}>Explore this feature</Text>
              </TouchableOpacity>
        </View>

        <View style={styles.whiteCard2}>
          <Text style={styles.subBelowText}>This feature enables you to filter hotels based on traveler amenities. It helps you refine your hotel search by selecting specific amenities or services that are important to you during your stay.</Text>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Explore this feature</Text>
          </TouchableOpacity>
        </View>

        </ScrollView>
        <NavigationBar title="Card 1" content="This is the content of Card 1." />
      </View>
    </View>
  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomRightRadius:60,
    borderBottomLeftRadius: 60,
  },
  welcomeCard: {
    backgroundColor: 'rgb(102, 153, 255)', // Change the color here
    padding: 20,
    height: 300,
    borderBottomRightRadius:60,
    borderBottomLeftRadius: 60,
    // borderBottomRightRadius: 80,
    // borderBottomLeftRadius: 280,
    shadowColor: 'black',  // Color of the shadow
    shadowOffset: { width: 0, height: 2 }, // Shadow offset
    shadowOpacity: 0.2,     // Shadow opacity
    shadowRadius: 6,       // Shadow radius
    elevation: 4, 
  },
  welcomeText: {
    fontSize: 30,
    marginTop: 75,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  subText: {
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 15,
    color: 'white',
  },
  subText1: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
  },
  subBelowText: {
      fontSize: 16,
      textAlign: 'center',
      marginBottom: 10,
      textAlign: 'justify'
    },
  whiteCardContainer: {
      marginBottom: 100,
      marginTop:100,
  },
  transparentCard: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)', // Adjust transparency as needed
    borderRadius: 10,
    padding:20,
    marginBottom: 100,
    marginTop:100,
    
    flex: 1,
  },

  mainCardText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 20,
  },
  whiteCardContainer: {
    flex: 1,
    marginTop: 20,
  },
  mwhiteCard:{
     
      borderRadius: 10,
      padding: 20,
      marginTop:12,
      marginBottom: -70,
      borderTopRightRadius: 35,
      borderTopLeftRadius: 35,
      height:620,


      shadowColor: 'black',  // Color of the shadow
      shadowOffset: { width: 0, height: 2 }, // Shadow offset
      shadowOpacity: 0.2,     // Shadow opacity
      shadowRadius: 4,       // Shadow radius
      elevation: 4, 
  },
  whiteCard: {
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderRadius: 10,
    padding: 20,
    marginBottom: 20,
    marginTop:30,
  },
  whiteCard2: {
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderRadius: 10,
    padding: 20,
    marginBottom: 20,
    marginTop:20,
  },
  whiteCardText: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#4287f5',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
},
});