import React from 'react';
import { View, Text, ScrollView, StyleSheet,SafeAreaView, Image } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Icon } from 'react-native-elements';
import { routehero } from "../assets";

const LocationDetails = () => {
  const route = useRoute();

  // Retrieve the selectedCategories passed from LocationDisplay
  const selectedCategories = route.params?.selectedCategories || [];
  const categoryData = route.params?.categoryData || {};

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
      <View style={{ flex: 1 }}>
     

      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
            <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
            <Text style={styles.welcomeText}>Welcome to Location Planing !</Text>
          <Text style={styles.subText}>Explore, Plan, and Enjoy Your Journey</Text>
          <Icon name="map-marker" type="font-awesome" size={60} color="white"  />
          </View>
    <ScrollView style={styles.container}>
      {selectedCategories.map((category, categoryIndex) => (
        <View key={categoryIndex} style={styles.categoryContainer}>
          <Text style={styles.categoryTitle}>{category}</Text>
          {categoryData[category] && (
            // Display details for the selected category
            <View style={styles.locationList}>
              {categoryData[category].map((location, index1) => (
                <View key={`${category}-${index1}`} style={styles.locationItem}>
                  <Text style={styles.locationName}>Name: {location.name}</Text>
                  <Text style={styles.locationText}>Latitude: {location.latitude}</Text>
                  <Text style={styles.locationText}>Longitude: {location.longitude}</Text>
                  {location.description && (
                    <Text style={styles.locationDescription}>Description: {location.description}</Text>
                  )}
                  {location.Image && (
                    <Image source={location.Image} style={styles.locationImage} />
                  )}
                </View>
              ))}
            </View>
          )}
        </View>
      ))}
    </ScrollView>
    </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: '#f0f0f0', // Background color for the entire screen
  },
  categoryContainer: {
    marginBottom: 20,
    padding: 16,
    backgroundColor: 'white', // Background color for each category section
    borderRadius: 10,
    elevation: 3, // Shadow effect for Android
    shadowColor: '#000', // Shadow color for iOS
    shadowOpacity: 0.2, // Shadow opacity for iOS
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  categoryTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  locationList: {},
  locationItem: {
    marginBottom: 16,
  },
  locationName: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  locationText: {
    fontSize: 16,
  },
  locationDescription: {
    fontSize: 16,
    fontStyle: 'italic',
    color: 'gray',
  },
  locationImage: {
    width: 200, 
    height: 150, 
    resizeMode: 'cover', 
    marginVertical: 10, 
  },
  welcomeText: {
    fontSize: 30,
    marginTop: -25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  subText: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'center',
    color: 'white',
  },
  // Add more styles for images, buttons, or other UI elements as needed
});

export default LocationDetails;
