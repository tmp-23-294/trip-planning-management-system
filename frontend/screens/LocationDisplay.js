import React, { useEffect, useState } from 'react';
import { View, StyleSheet,TouchableOpacity,FlatList,Image,Text} from 'react-native';
import MapView,{ Marker } from 'react-native-maps';
import axios from 'axios';
import { FontAwesome5 } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { locations } from "../assets";
import { useNavigation } from "@react-navigation/native";


const LocationDisplay = () => {
  const navigation = useNavigation();
  const [result, setResult] = useState(null);
  const [selectedCategories, setSelectedCategories] = useState([])
    

  const [church, setChurch] = useState([

    //churches
    { name: 'Lifeway Church Assembly of God',Image: { uri: 'https://i.postimg.cc/zBJ2pWGF/2018-02-04.jpg' }, latitude: 6.0634722113298745, longitude: 80.19912011980679 ,description:"Lifeway Church Assembly of God is a non-denominational Christian church located in the heart of Galle.The church was founded in 1978 by a group of believers who were passionate about sharing the love of Jesus Christ with others." },
    { name: 'SL Christian',Image: { uri: 'https://i.postimg.cc/QCXy0zjt/sl-christian.jpg' }, latitude: 6.038083258441641, longitude: 80.22102242030705 ,description: "The church believes that love is the foundation of all relationships, and it strives to show love to everyone, regardless of their background or beliefs.,Address: 26QC+6C8, Olcet road, Galle 80000, Phone: 071 999 3499"},
    { name: "St Joseph's Chapel'",Image: { uri: 'https://i.postimg.cc/cHTm4LNx/St-Joseph-s-Chapel.jpg' }, latitude: 6.029036564651874, longitude: 80.21611877480503, description:"St. Joseph's Chapel is a Catholic church located in the heart of Galle Fort.The church was built in 1797 by the Portuguese, and it is dedicated to Saint Joseph, the husband of Mary.The church is a small, single-room structure with a whitewashed exterior and a red-tiled roof"},
    
  ]);
  
  const [beach, setBeach] = useState([
    //beaches
    { name: 'Lighthouse Beach',Image: { uri: 'https://i.postimg.cc/xj4vs7f4/Lighthouse-Beach.webp' }, latitude: 6.0247193863143345, longitude: 80.21948850152249, description: "Lighthouse Beach is a popular tourist destination in Galle, Sri Lanka. It is located at the eastern end of Galle Fort, and it is known for its white sand beach, clear waters, and lighthouse.The beach is a popular spot for swimming, sunbathing, and surfing. There are also a few restaurants and cafes located near the beach." },
    { name: 'Jungle Beach, Unawatuna.', Image: { uri: 'https://i.postimg.cc/pTdjCQgv/Jungle-Beach-Unawatuna.jpg' },latitude: 6.018748977706611, longitude: 80.23942639541931, description: "Jungle Beach is a secluded beach located west of Unawatuna.It is known for its golden sand, clear waters, and lush vegetation.The beach is surrounded by cliffs and jungle, which gives it a more secluded and private feel than other beaches in the area" },
    { name: 'Unawatuna Beach', Image: { uri: 'https://i.postimg.cc/43W7Y7nS/Unawatuna-Beach.jpg' },latitude: 6.009729981936647, longitude: 80.2484270774404,description:"Unawatuna Beach is a popular tourist destination known for its golden sand, clear waters, and coral reefs.It is located about 5 kilometers (3.1 mi) southeast of Galle Fort.The beach is a popular spot for swimming, sunbathing, surfing, and snorkeling.There are also a few resorts and restaurants located near the beach"},
   
  ]);

  const [parks, setParks] = useState([
    //parks
    { name: 'Mahamodara Beach Park & Marine Walk',Image: { uri: 'https://i.postimg.cc/8Cpt60mP/Mahamodara-Beach-Park-Marine-Walk.jpg' }, latitude: 6.040187559667304, longitude: 80.20030527682457,description:" Mahamodara Beach Park & Marine Walk is a beautiful park located in Galle, Sri Lanka. It is a popular spot for locals and tourists alike, and it is known for its scenic views, lush greenery, and variety of activities.The park is located on the coast of the Indian Ocean, and it offers stunning views of the beach and the surrounding area" },
    { name: 'Galle municipal park seashow',Image: { uri: 'https://i.postimg.cc/3x7Zsc6C/Galle-municipal-park-seashow.jpg' }, latitude: 6.030164158592169, longitude: 80.2171365509482, description:"Address: 26J8+3V6, Baladaksha Mawatha,Galle municipal park seashow is a Garden located in Galle 80000. It is one of the 373 gardens in Sri Lanka. Address of Galle municipal park seashow is Baladaksha Mawatha, Galle 80000, Sri Lanka" },
    { name: 'Darmapala Park', Image: { uri: 'https://i.postimg.cc/0NXdJ2Nc/Darmapala-Park.jpg' },latitude: 6.032158029015325, longitude: 80.21365345665617,description:"Dharmapala Park is a public park located in Galle, Sri Lanka. It is situated near the historic Galle Fort, and it is a popular spot for locals and tourists alike.The park is named after Anagarika Dharmapala, a Buddhist monk who was a leading figure in the revival of Buddhism in Sri Lanka. The park was established in 1953, and it was renovated in 2022"},

  ]);

  const [theatres, setTheatres] = useState([
    //theatres
    { name: 'Prince Cinema', Image: { uri: 'https://i.postimg.cc/zvKnMCFc/Prince-Cinema.jpg' },latitude: 6.035061045482065, longitude: 80.21499309006786, description:"Prince Cinema is a single-screen cinema located in Kaluwella, Galle, Sri Lanka. It is a small, family-owned cinema that has been in operation since the 1970s.The cinema has one auditorium with a capacity of about 200 people. It shows a mix of local and international films. The cinema is not air-conditioned, but it is a popular spot for locals and tourists alike." },
    { name: "Queen's Cinema", Image: { uri: 'https://i.postimg.cc/xTPmzTxM/Queens-Cinema.jpg' },latitude: 6.03420346237629, longitude: 80.21666556784687,description:"Queen's Cinema is a single-screen cinema located in Wakwella Road, Galle, Sri Lanka. It is a small, family-owned cinema that has been in operation since the 1960s.The cinema has one auditorium with a capacity of about 418 people. It shows a mix ,The Queen's Cinema is a great place to watch a movie on a budget. Tickets are only 150 LKR (about $1 USD). The cinema is open from 6pm to 10pm daily." },

  ]);

  const [museums, setMuseums] = useState([
    //museums
    { name: 'Maritime Museum',Image: { uri: 'https://i.postimg.cc/3J9ywfNZ/Maritime-Museum.jpg' }, latitude: 6.028106202756581, longitude: 80.21842673347102, description:"The Maritime Museum is located in Galle Fort, Sri Lanka. It is housed in a 1671 Dutch warehouse, and it was opened to the public in 1992.The museum exhibits a variety of artifacts related to the maritime history of Sri Lanka, including ship models, maps, navigational tools, and nautical instruments. There are also exhibits on the history of the spice trade, and the role of Sri Lanka as a maritime hub in the Indian Ocean." },
    { name: 'National Museum Galle',Image: { uri: 'https://i.postimg.cc/sXFxLtH3/National-Museum-Galle.jpg' }, latitude: 6.028917166578293, longitude: 80.21687822942249,description:"The National Museum of Galle is one of the national museums of Sri Lanka. It is located in the oldest remaining Dutch building in the Galle fort, a single storey colonnaded Dutch building built in 1656 as the commissariat store for the Dutch garrison at the fort. It subsequently served as a billiards room for the adjoining New Oriental Hotel (now the Amangalla Hotel). The building was renovated by the Department of National Museums and opened on 31 March 1986." },
    { name: 'Under Water Museum', Image: { uri: 'https://i.postimg.cc/Y0rrKk6B/Under-Water-Museum.jpg' },latitude: 6.020707898371897, longitude: 80.21575987713557,description:"The Underwater Museum is located in Galle, Sri Lanka. It is the first underwater museum in Sri Lanka and was opened in 2020. The museum is located in the Galle harbor and it is home to over 50 sculptures of marine life, including turtles, fish, and coral reefs"},

  ]);

  const [malls, setMalls] = useState([
   //malls
   { name: 'Galle City Centre', latitude: 6.037246098682479, longitude: 80.22489913467945,description:"Galle City Centre is the heart of Galle, Sri Lanka. It is a UNESCO World Heritage Site and is known for its well-preserved Dutch colonial architecture. The city center is home to a number of historical buildings, including the Galle Fort, the National Museum of Galle, and the Maritime Museum" },
   { name: 'Pink Arcade', latitude: 6.033328246788638, longitude: 80.21557490091428,description:"Pink Arcade is a fashion and beauty shop located in Galle City Centre. It is a one-stop shop for all your beauty needs, from cosmetics and skincare to hair and nail products."},

  ]);

  const [art, setArt] = useState([
    //art_galleries
    { name: 'Arya Art at Galle Fort Ambalama', Image: { uri: 'https://i.postimg.cc/jqN056JL/Arya-Art-at-Galle-Fort-Ambalama.jpg' },latitude: 6.027935545823699, longitude: 80.21903442589783, description:"Arya Art at Galle Fort Ambalama is a contemporary art gallery located in the heart of Galle Fort. It was founded in 2015 by Arya Weerasinghe, a Sri Lankan artist and curator" },
    { name: 'Lanka Living Gallery', Image: { uri: 'https://i.postimg.cc/zBJ2pWGF/2018-02-04.jpg' },latitude: 6.026063354583896, longitude: 80.2172467922688, description:"Lanka Living Gallery is an art gallery located in Galle, Sri Lanka. It was founded in 2007 by Tomas Clausen, a Danish artist and Gallerist.The gallery showcases a variety of art from Sri Lanka and around the world. The artworks include paintings, sculptures, photographs, and textiles." },
    { name: 'Sakura Art Store & Gallery',Image: { uri: 'https://i.postimg.cc/8PRbjcHR/Sakura-Art-Store-Gallery-galle-images.jpg' }, latitude: 6.035365074999641, longitude: 80.21720472957782, description:"Sakura Art Store & Gallery is an art gallery and art supplies store located in Galle, Sri Lanka. It was founded in 1983 by Sakura Karunaratne, a Sri Lankan artist and entrepreneur."},
   ]);

   
   const [swim, setSwim] = useState([
    //swimming_pools
   { name: 'Le Grand Galle Pool', Image: { uri: 'https://i.postimg.cc/gctFkvdZ/Le-Grand-Galle-Pool.jpg' },latitude: 6.031998033306635, longitude: 80.21237208712738,description:"The Le Grand Galle Pool is a beautiful infinity pool located in the Le Grand Galle hotel. It is situated on the rooftop of the hotel and offers stunning views of the Galle Fort and the Indian Ocean" },
   { name: "St.Aloysius' College Swimming Pool",Image: { uri: 'https://i.postimg.cc/5NvV7XdQ/St-Aloysius-College-Swimming-Pool.jpg' }, latitude: 6.040857854660651, longitude: 80.21406363323531, description:"The St. Aloysius' College Swimming Pool is a 50-meter Olympic-sized swimming pool located in Galle, Sri Lanka. It is owned and operated by St. Aloysius' College, a prestigious boys' school in Galle" },
   { name: 'SC Galle', Image: { uri: 'https://i.postimg.cc/2jLRy159/SC-Galle.jpg' },latitude: 6.058736338016319, longitude: 80.2071185792926,description:"SC Galle is a guesthouse in Galle, Sri Lanka. It has a 25-meter, 6-lane open-air swimming pool. The pool is located in the garden of the guesthouse and is surrounded by palm trees" },
   ]);

   const [gym, setGym] = useState([
   //gyms
   { name: 'DSP Y Zone Gym',Image: { uri: 'https://i.postimg.cc/BnqVTbq0/DSP-Y-Zone-Gym.jpg' }, latitude: 6.060209586748769, longitude: 80.20201165941744,description:"It is located at 110/1/1 Wakwella Rd, Galle, Sri Lanka,It is open from 5:30 AM to 10:00 PM daily,It has a variety of equipment, including treadmills, ellipticals, weight machines, free weights, and yoga mats." },
   { name: "Ruhunu Gym & Fitness Centre",Image: { uri: 'https://i.postimg.cc/yNDnjStw/Ruhunu-Gym-Fitness-Centre.jpg' }, latitude: 6.06859795733849, longitude: 80.22900302242756,description:"It is located at No.241, Karapitiya Rd, Galle, Sri Lanka,It is open from 6:00 AM to 10:00 PM daily,It has a variety of equipment, including treadmills, ellipticals, weight machines, free weights, and yoga mats" },
   { name: 'CJ Gym & Fitness Centre',Image: { uri: 'https://i.postimg.cc/sDfYKhZw/CJ-Gym-Fitness-Centre.jpg' }, latitude: 6.0548895662865005, longitude: 80.2571087823623, description:"It is located at Ginigalthuduwa, Walahanduwa, Galle, Sri Lanka.It is open from 5:00 AM to 10:00 PM daily.It also has a sauna and a steam room"},
   ]);

   const [view, setView] = useState([
     //view_points
     { name: 'Galle Fort Attractions and Jumpers Sri Lanka', Image: { uri: 'https://i.postimg.cc/zfq8R8Qb/Galle-Fort-Attractions-and-Jumpers-Sri-Lanka.jpg' },latitude:  6.0237485305582545, longitude: 80.21750776857387,description:"The Galle Fort Attractions and Jumpers are a group of people who perform dangerous stunts on the ramparts of Galle Fort in Sri Lanka. They are known for jumping off the walls of the fort, sometimes as high as 40 feet, into the ocean below" },
     { name: "View Point - Mahamodara",Image: { uri: 'https://i.postimg.cc/tJJSGjP1/View-Point-Mahamodara.jpg' }, latitude:   6.038892277020359, longitude: 80.19859195139522,description:"View Point - Mahamodara is a viewpoint located in Mahamodara, Sri Lanka. It is a popular tourist destination for its stunning views of the surrounding area, including the Galle Fort, the Indian Ocean, and the hills of the Southern Province" },
     { name: 'Lighthouse View Point',Image: { uri: 'https://i.postimg.cc/NjCvLmRV/Lighthouse-View-Point.webp' }, latitude: 6.024521293345759, longitude: 80.21951063113247,description:"Lighthouse View Point is a scenic spot in Galle Fort, Sri Lanka. It is located on the southern end of the fort and offers stunning views of the Indian Ocean, the Galle Harbor, and the Galle Fort itself. " },
    ]);

    const [gardens, setGardens] = useState([
      //gardens
      { name: 'Jagoda Villa Garden', Image: { uri: 'https://i.postimg.cc/J0M2pzbZ/Jagoda-Villa-Garden.jpg' },latitude:  6.03907558401868, longitude: 80.24409275102789,description:"miniature gardens that feature trees and shrubs that have been trained to grow in small containers. They are often found in Asian cultures, but they are becoming increasingly popular in other parts of the world" },
      { name: "Galle Lanka Bonsai", Image: { uri: 'https://i.postimg.cc/RV8TF0Nd/galle-bonsay.png' },latitude: 6.0616726352428225, longitude: 80.18208268210756,description:"Galle Lanka Bonsai is a garden in Galle, Sri Lanka that specializes in bonsai trees. It is located in the heart of the city, near the Galle Fort.The garden was founded in 1999 by Chamila Soysa, a renowned bonsai artist. Soysa has been training in bonsai for over 30 years and has won numerous awards for his work" },
      { name: 'Galla',Image: { uri: 'https://i.postimg.cc/9MWdSRnm/galla.jpg' }, latitude: 6.053519893623236, longitude: 80.22097973192206,description:"are designed for beauty and aesthetic appeal. They often feature a variety of flowers, shrubs, and trees, as well as fountains, statues, and other decorative elements." },
     ]);

    
    

     const categoryMap = {
      churches : <FontAwesome5 name="church" size={24} color="black" />,
      beaches: <FontAwesome5 name="umbrella-beach" size={24} color="black" />,
      swimming_pools: <MaterialIcons name="pool" size={24} color="black" />,
      parks: <MaterialIcons name="park" size={24} color="black" />,
      theatres: <Fontisto name="film" size={24} color="black" />,
      museums: <MaterialIcons name="museum" size={24} color="black" />,
      malls: <MaterialIcons name="local-mall" size={24} color="black" />,
      art_galleries: <MaterialCommunityIcons name="view-gallery-outline" size={24} color="black" />,
      gyms: <FontAwesome5 name="dumbbell" size={24} color="black" />,
      view_points: <FontAwesome5 name="street-view" size={24} color="black" />,
      gardens: <FontAwesome5 name="campground" size={24} color="black" />,

     };

    //  const CategoryNamesDisplay = ({ selectedCategories }) => {
    //   return (
    //     <View style={styles.categoryNamesContainer}>
    //       {selectedCategories.map((category, index) => (
    //         <Text key={index} style={styles.categoryName}>
    //           {category}
    //         </Text>
    //       ))}
    //     </View>
    //   );
    // };
    


  useEffect(()=>{

    function DisplayResult() {
      
        axios

          .get("http://16.171.19.240:5000/display")

          .then((res) => {
           console.log(res.data);
           setResult(res.data);
           const modelOutput = res.data.placesflask.result; 
           setSelectedCategories(modelOutput);
           
          })

          .catch((err) => {

            alert(err.msg);

          })
      }

      DisplayResult();
    }, []);

    const filterSelectedCategories = (selectedCategories, categoryData) => {
      // Filter the selectedCategories array
      return selectedCategories.filter(category => category in categoryData);
    };

    const filteredCategories = filterSelectedCategories(selectedCategories, {
      churches: church,
      beaches: beach,
      swimming_pools: swim,
      parks: parks,
      theatres: theatres,
      museums: museums,
      malls: malls,
      art_galleries: art,
      gyms: gym,
      view_points: view,
      gardens: gardens
    });

    
    
  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 6.0305055127702785,
          longitude:  80.21502049054205,
          latitudeDelta: 0.01,
          longitudeDelta: 0.01
        }}
      >
        
         {selectedCategories.flatMap((category,categoryIndex) =>
           categoryMap[category] ? (
            category === "churches" ?
            
          
            church.map((church, index1) => (
          <Marker
            key={`church-${index1}`}
            coordinate={{
              latitude: church.latitude,
              longitude: church.longitude
            }}
            title={church.name}
            
          >

            <FontAwesome5 name="church" size={24} color="black" />
            
          </Marker>
          
          )) :
            category === "beaches" ?
            beach.map((beach, index2) => (
          <Marker
            key={`beach-${index2}`}
            coordinate={{
              latitude: beach.latitude,
              longitude: beach.longitude
            }}
            title={beach.name}
          >
           
             
            <FontAwesome5 name="umbrella-beach" size={24} color="black" />
            
            </Marker>
            )) :
            
            category === "swimming_pools" ?
            swim.map((swim, index3) => (
          <Marker
            key={`swim-${index3}`}
            coordinate={{
              latitude: swim.latitude,
              longitude: swim.longitude
            }}
            title={swim.name}
          >
             
            <MaterialIcons name="pool" size={24} color="black" />,
            
            </Marker>
            )):    

            category === "parks" ?
            parks.map((parks, index4) => (
          <Marker
            key={`parks-${index4}`}
            coordinate={{
              latitude: parks.latitude,
              longitude: parks.longitude
            }}
            title={parks.name}
          >
           
            <MaterialIcons name="park" size={24} color="black" />
            
            </Marker>
            )): 

            category === "theatres" ?
            theatres.map((theatres, index5) => (
          <Marker
            key={`theatres-${index5}`}
            coordinate={{
              latitude: theatres.latitude,
              longitude: theatres.longitude
            }}
            title={theatres.name}
          >
            <Fontisto name="film" size={24} color="black" />
            
            </Marker>
            )): 

            category === "museums" ?
            museums.map((museums, index6) => (
          <Marker
            key={`museums-${index6}`}
            coordinate={{
              latitude: museums.latitude,
              longitude: museums.longitude
            }}
            title={museums.name}
          >
            
            <MaterialIcons name="museum" size={24} color="black" />
            
            </Marker>
            )): 

            category === "malls" ?
            malls.map((malls, index7) => (
          <Marker
            key={`malls-${index7}`}
            coordinate={{
              latitude: malls.latitude,
              longitude: malls.longitude
            }}
            title={malls.name}
          >
            
            <MaterialIcons name="local-mall" size={24} color="black" />
            
            </Marker>
            )): 

            category === "art_galleries" ?
            art.map((art, index8) => (
          <Marker
            key={`art-${index8}`}
            coordinate={{
              latitude: art.latitude,
              longitude: art.longitude
            }}
            title={art.name}
          >
            
            <MaterialCommunityIcons name="view-gallery-outline" size={24} color="black" />
            
            </Marker>
            )): 

            category === "gyms" ?
            gym.map((gym, index9) => (
          <Marker
            key={`gym-${index9}`}
            coordinate={{
              latitude: gym.latitude,
              longitude: gym.longitude
            }}
            title={gym.name}
          >
            
            <FontAwesome5 name="dumbbell" size={24} color="black" />
            
            </Marker>
            )): 

            category === "view_points" ?
            view.map((view, index10) => (
          <Marker
            key={`view-${index10}`}
            coordinate={{
              latitude: view.latitude,
              longitude: view.longitude
            }}
            title={view.name}
          >
            
           <FontAwesome5 name="street-view" size={24} color="black" />
            
            </Marker>
            )): 

            category === "gardens" ?
            gardens.map((gardens, index11) => (
          <Marker
            key={`gardens-${index11}`}
            coordinate={{
              latitude: gardens.latitude,
              longitude: gardens.longitude
            }}
            title={gardens.name}
          >
           
           <FontAwesome5 name="campground" size={24} color="black" />
            
            </Marker>
            )): 

            
                          


            []
          ):[]  
            )}   
            {/* </View>                                                                                                                                                                  */}
      </MapView>
      <Image source={locations}
        style={styles.imageOverlay}
      />
      <TouchableOpacity
        onPress={() =>
          navigation.navigate("LocationDetails", {
            selectedCategories: selectedCategories,
            categoryData: {
              churches: church,
              beaches: beach,
              swimming_pools: swim,
              parks: parks,
              theatres: theatres,
              museums: museums,
              malls: malls,
              art_galleries: art,
              gyms: gym,
              view_points: view,
              gardens: gardens

            },
          })
        }
        
        style={styles.navigateButton}
      >
        <Text style={styles.buttonText}>Details</Text>
      </TouchableOpacity>
      <View style={styles.categoryListContainer}>
      <FlatList
        horizontal
        contentContainerStyle={styles.categoryList}
        data={filteredCategories}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View style={styles.categoryNameContainer}>
            {categoryMap[item]}
            <Text> {item}</Text>
          </View>
        )}
      />
     </View>
     </View>
    
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  imageOverlay: {
    position: 'absolute',
    top: 5,
    left: 0,
    width: 100, // Set the width of the image
    height: 385, // Set the height of the image
    resizeMode: 'cover', // Adjust the image content to cover the designated area
    borderRadius: 20,
    margin: 20, // Adjust the margin to position the image as needed
  },
  categoryNameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 4,
    padding: 9,
    borderRadius: 4,
    backgroundColor: 'white',
    elevation: 3,
    marginRight: 8, // Add some margin between list items
  },
  categoryName: {
    marginLeft: 4,
    fontSize: 16,
    fontWeight: 'bold',
  },
  categoryList: {
    paddingHorizontal: 10, // Add horizontal padding to the list container
    maxHeight: 50,
     // Limit the maximum height of the list
  },
  navigateButton: {
    backgroundColor: 'black',
    borderRadius: 50, // Make it a circle
    width: 60, // Adjust the width and height to make it circular
    height: 80,
    position: 'absolute',
    bottom: 20, // Adjust the bottom and right values to position it
    right: 20,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 5, // Add elevation (shadow) for better visibility
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  }
});

export default LocationDisplay;

