import React, { useState, useEffect, useLayoutEffect } from "react";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  ActivityIndicator,
  SafeAreaView,
  Image,
} from "react-native";

import { routehero } from "../assets";
import NavigationBar from "../components/NavigationBar";
import { Icon } from 'react-native-elements';

const preferences = [
  "churches",
  "resorts",
  "beaches",
  "parks",
  "theatres",
  "museums",
  "malls",
  "zoo",

  "restaurants",
  "pubs_bars",
  "local_services",
  "burger_pizza_shops",
  "hotels_other_lodgings",

  "juice_bars",
  "art_galleries",
  "dance_clubs",
  "swimming_pools",
  "gyms",
  "bakeries",

  "beauty_spas",
  "cafes",
  "view_points",
  "monuments",
  "gardens",
];

export default function LocationHomePage() {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const [preferencesData, setPreferencesData] = useState({});
  const [count, setCount] = useState();
  const [userId, setUserId] = useState("");
  const [churches, setChurches] = useState(0);
  const [resorts, setResorts] = useState(0);
  const [beaches, setBeaches] = useState(0);
  const [parks, setParks] = useState(0);
  const [theatres, setTheatres] = useState(0);
  const [museums, setMuseums] = useState(0);
  const [malls, setMalls] = useState(0);
  const [zoo, setZoo] = useState(0);
  const [restaurants, setRestaurants] = useState(0);
  const [pubs_bars, setPubs_bars] = useState(0);
  const [local_services, setLocal_services] = useState(0);
  const [burger_pizza_shops, setBurger_pizza_shops] = useState(0);
  const [hotels_other_lodgings, setHotels_other_lodgings] = useState(0);
  const [juice_bars, setJuice_bars] = useState(0);
  const [art_galleries, setArt_galleries] = useState(0);
  const [dance_clubs, setDance_clubs] = useState(0);
  const [swimming_pools, setSwimming_pools] = useState(0);
  const [gyms, setGyms] = useState(0);
  const [bakeries, setBakeries] = useState(0);
  const [beauty_spas, setBeauty_spas] = useState(0);
  const [cafes, setCafes] = useState(0);
  const [view_points, setView_points] = useState(0);
  const [monuments, setMonuments] = useState(0);
  const [gardens, setGardens] = useState(0);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  useEffect(() => {
    function getRecordNumber() {
      axios

        .get("http://16.171.19.240:5000/csv_record_count")

        .then((res) => {
          console.log(res.data);
          setCount(res.data.count);
        })

        .catch((err) => {
          alert(err.msg);
        });
    }

    getRecordNumber();
  }, []);

  const save = () => {
    setIsLoading(true);
    const combinedData = {
      User: "User " + count,
      "Category 1": churches,
      "Category 2": resorts,
      "Category 3": beaches,
      "Category 4": parks,
      "Category 5": theatres,
      "Category 6": museums,
      "Category 7": malls,
      "Category 8": zoo,
      "Category 9": restaurants,
      "Category 10": pubs_bars,
      "Category 11": local_services,
      "Category 12": burger_pizza_shops,
      "Category 13": hotels_other_lodgings,
      "Category 14": juice_bars,
      "Category 15": art_galleries,
      "Category 16": dance_clubs,
      "Category 17": swimming_pools,
      "Category 18": gyms,
      "Category 19": bakeries,
      "Category 20": beauty_spas,
      "Category 21": cafes,
      "Category 22": view_points,
      "Category 23": monuments,
      "Category 24": gardens,
    };

    console.log(combinedData);

    axios

      .post("http://16.171.19.240:5000/code", combinedData)

      .then((res) => {
        console.log(res.data);
        model();
      })

      .catch((err) => {
        setIsLoading(false);
        alert(err.msg);
      });
  };

  function model() {
    let userId = "User " + count;
    console.log("userId", userId);
    const newData = {
      User: "User " + count,
    };
    axios
      .post("http://16.171.19.240:5000/model", newData)

      .then((res) => {
        setIsLoading(false);
        console.log(res.data);
        navigation.navigate("LocationDisplay");
      })

      .catch((err) => {
        setIsLoading(false);
        alert(err.msg);
      });
  }

  const handlePreferenceChange = (preference, value) => {
    setPreferencesData((prevData) => ({
      ...prevData,

      [preference]: value,
    }));
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#333" />
      </View>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
      <View style={{ flex: 1 }}>
     

      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
            <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
            <Text style={styles.welcomeText}>Welcome to Location Planing !</Text>
          <Text style={styles.subText}>Explore, Plan, and Enjoy Your Journey</Text>
          <Icon name="map-marker" type="font-awesome" size={60} color="white"  />
          </View>

        <View style={styles.container}>
          <ScrollView contentContainerStyle={styles.scrollViewContent}>
            <Text style={styles.heading}>Preference Points</Text>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>churches</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={churches.toString()}
                keyboardType="numeric"
                onChangeText={setChurches}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>resorts</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={resorts.toString()}
                keyboardType="numeric"
                onChangeText={setResorts}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>beaches</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={beaches.toString()}
                keyboardType="numeric"
                onChangeText={setBeaches}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>parks</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={parks.toString()}
                keyboardType="numeric"
                onChangeText={setParks}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>theatres</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={theatres.toString()}
                keyboardType="numeric"
                onChangeText={setTheatres}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>museums</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={museums.toString()}
                keyboardType="numeric"
                onChangeText={setMuseums}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>malls</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={malls.toString()}
                keyboardType="numeric"
                onChangeText={setMalls}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>zoo</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={zoo.toString()}
                keyboardType="numeric"
                onChangeText={setZoo}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>restaurants</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={restaurants.toString()}
                keyboardType="numeric"
                onChangeText={setRestaurants}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>pubs_bars</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={pubs_bars.toString()}
                keyboardType="numeric"
                onChangeText={setPubs_bars}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>local_services</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={local_services.toString()}
                keyboardType="numeric"
                onChangeText={setLocal_services}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>burger_pizza_shops</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={burger_pizza_shops.toString()}
                keyboardType="numeric"
                onChangeText={setBurger_pizza_shops}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>hotels_other_lodgings</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={hotels_other_lodgings.toString()}
                keyboardType="numeric"
                onChangeText={setHotels_other_lodgings}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>juice_bars</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={juice_bars.toString()}
                keyboardType="numeric"
                onChangeText={setJuice_bars}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>art_galleries</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={art_galleries.toString()}
                keyboardType="numeric"
                onChangeText={setArt_galleries}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>dance_clubs</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={dance_clubs.toString()}
                keyboardType="numeric"
                onChangeText={setDance_clubs}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>swimming_pools</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={swimming_pools.toString()}
                keyboardType="numeric"
                onChangeText={setSwimming_pools}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>gyms</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={gyms.toString()}
                keyboardType="numeric"
                onChangeText={setGyms}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>bakeries</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={bakeries.toString()}
                keyboardType="numeric"
                onChangeText={setBakeries}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>beauty_spas</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={beauty_spas.toString()}
                keyboardType="numeric"
                onChangeText={setBeauty_spas}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>cafes</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={cafes.toString()}
                keyboardType="numeric"
                onChangeText={setCafes}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>view_points</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={view_points.toString()}
                keyboardType="numeric"
                onChangeText={setView_points}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>monuments</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={monuments.toString()}
                keyboardType="numeric"
                onChangeText={setMonuments}
              />
            </View>

            <View style={styles.preferenceContainer}>
              <Text style={styles.preferenceLabel}>gardens</Text>

              <TextInput
                placeholder="Input Points"
                placeholderTextColor="#ccc"
                style={styles.preferenceInput}
                value={gardens.toString()}
                keyboardType="numeric"
                onChangeText={setGardens}
              />
            </View>

            <View>
              <Button title="Save" onPress={save} />
            </View>
          </ScrollView>
        </View>
      </View>
      <NavigationBar />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    padding: 20,

    backgroundColor: "#fff",
  },

  scrollViewContent: {
    alignItems: "center",
  },

  heading: {
    fontSize: 24,

    fontWeight: "bold",

    marginBottom: 20,
  },

  preferenceContainer: {
    flexDirection: "row",

    alignItems: "center",

    marginBottom: 10,
  },

  preferenceLabel: {
    flex: 1,

    fontSize: 16,
  },

  preferenceInput: {
    flex: 1,

    height: 40,

    borderColor: "gray",

    borderWidth: 1,

    paddingLeft: 10,
  },

  loadingContainer: {
    alignItems: "center",
    marginTop: 20,
  },
  loadingContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  welcomeText: {
    fontSize: 30,
    marginTop: -25,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  subText: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'center',
    color: 'white',
  },
});

