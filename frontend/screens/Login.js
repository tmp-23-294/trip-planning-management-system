import React, { useState, useLayoutEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Dimensions, Image } from 'react-native';
import { Icon } from 'react-native-elements';
import axios from 'axios';
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { user } from "../assets";
const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();
  const logo = require('../assets/logo.png');

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);
  
  const handleLogin = (e) => {
 
    e.preventDefault();
    const loginCredentials = {
      username,
      password
    }

    axios.post("http://16.171.19.240:5000/login", loginCredentials).then((res)=>{
      
    console.log("hello");
     AsyncStorage.setItem("username", username);

     console.log(res.data);
     
     console.log(loginCredentials);
    
     console.log("hello");
     navigation.navigate('HomeScreen');
     
    }).catch((err) =>{

   console.log(err.response.data);
    alert("Invalid login");
  })
  };

  return (
    <View style={styles.container}>
          <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Image source={user} style={{ width: "120%", height: 800, resizeMode: "cover", position: "absolute", top: 35 }} />
            <View className="flex-row  mt-6 items-center justify-center space-x-2">
      
      <Image source={logo} style={styles.logoImage} />
    

       
      </View>
          </View>
          
      <View style={styles.card1}>
      <Text style={styles.logo} >Login</Text>

      <View style={styles.inputView}>
        <Icon name="user" type="font-awesome" size={24} color="#ccc" style={styles.inputIcon} />
        <TextInput
          placeholder="Username"
          placeholderTextColor="#ccc"
          style={styles.inputText}
          value={username}
          onChangeText={(text) => setUsername(text)}
        />
      </View>

      <View style={styles.inputView}>
        <Icon name="lock" type="font-awesome" size={24} color="#ccc" style={styles.inputIcon} />
        <TextInput
          placeholder="Password"
          placeholderTextColor="#ccc"
          secureTextEntry
          style={styles.inputText}
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
      </View>

      <TouchableOpacity style={styles.loginButton} onPress={handleLogin}>
        <Text style={styles.loginButtonText}>Login</Text>
      </TouchableOpacity>
      <View style={{ alignItems: "center", height: 100 }}>
            <Text >
              If you don't have a account?
            </Text>
            <TouchableOpacity
              style={{ margin: 0 }}
              onPress={() => {
                 navigation.navigate('Createaccount');
              }}
            >
              <Text
                style={{ fontSize: 16, margin: 8 }}
              >
                SIGN UP
              </Text>
            </TouchableOpacity>
          </View>
          </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    fontSize: 32,
    marginBottom: 50,
  },
  inputView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    width: '80%',
  },
  inputIcon: {
    marginRight: 10,
  },
  inputText: {
    flex: 1,
    height: 30,
    color: '#000',
  },
  loginButton: {
    marginTop: 20,
    backgroundColor: '#007BFF',
    borderRadius: 5,
    paddingVertical: 12,
    paddingHorizontal: 30,
    marginBottom :20
  },
  loginButtonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  card1: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    marginBottom:100,
    padding: 16,
    marginVertical: 5,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 3,
    width: Dimensions.get("window").width * 0.9,
    height: Dimensions.get("window").height * 0.6,
    justifyContent: "center",
    alignItems: "center",
  },
  logoImage: {
    width: 120,
    height: 120,
    borderRadius: 60, // Use half of the width for rounded corners to make it a circle
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Login;
