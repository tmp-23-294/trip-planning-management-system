import {
  View,
  Text,
  SafeAreaView,
  Image,
  TextInput,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions
} from "react-native";
import React, { useEffect, useLayoutEffect, useState } from "react";
import * as Animatable from "react-native-animatable";
import { useNavigation } from "@react-navigation/native";
import { Avatar } from "../assets";
import { location } from "../assets";
import { RouteIm } from "../assets";
import { loca } from "../assets";
import { hotel } from "../assets";
import { carni } from "../assets";
import { heroImage } from "../assets";
import NavigationBar from '../components/NavigationBar';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Amplify, Storage } from 'aws-amplify';
import awsconfig from '../src/aws-exports';
Amplify.configure(awsconfig);
import AsyncStorage from "@react-native-async-storage/async-storage";

const MainScreen = () => {
  const [s3Image, setS3Image] = useState(null);
const navigation = useNavigation();
const [inputFields, setInputFields] = useState(1);
const heroImageUrl = 'https://i.postimg.cc/hvz6F635/wp9571447-1.jpg';

useEffect(() => {
  navigation.setOptions({
    headerShown: false,
  });

  async function getUserData() {
    const username = await AsyncStorage.getItem("username");

    try {
      const imageURI = await Storage.get(`profPic/${username}/${username}.jpg`);
      setS3Image(imageURI);
    } catch (error) {
      console.log(error);
    }
  }

 getUserData();
}, []);


  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);
  const navigateToScreen = (screenName) => {
    // You can navigate to the desired screen here using navigation.navigate()
    // Example: navigation.navigate(screenName);
  };
 
return (
  
    <SafeAreaView style={{ flex: 1, paddingTop: 50, backgroundColor: "#e6eeff" }}>
     
      <View style={styles.heroImageContainer}>
      <Image source={{ uri: heroImageUrl }} style={styles.heroImage} />

      <View style={styles.overlay}>
  <Text style={styles.welcomeText}>Welcome to Traveller!</Text>
  <Text style={styles.subText}>Explore, Plan, and Enjoy Your Journey</Text>
</View>
      </View>
    <View className="flex-row items-center justify-between px-8">
      <View>

      </View>
    <TouchableOpacity onPress={() => navigation.navigate("Usersdetails")}>
      <View className=" bg-gray-400 rounded-full items-center justify-center shadow-lg"style={styles.avatarContainer}>
      {s3Image && (
          <Image source={{ uri: s3Image }} style={styles.profileImage} />
        )}
    </View>
    {/* <View style={styles.searchBarContainer}>
<View style={styles.searchBar}>
  <Icon name="search" size={20} color="black" style={styles.searchIcon} />
  <TextInput
    style={styles.searchText}
    placeholder="Search..."
    placeholderTextColor="black"
  />
</View>
</View> */}

  </TouchableOpacity>    
    </View>
   
    
    <ScrollView>
    <View style={styles.redCard}>
    <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20 }}>
<View style={styles.cardContainer}>
  <TouchableOpacity onPress={() => navigation.navigate("LocationHomePage")} style={styles.card}>
    <Image source={loca} style={styles.image} />
    <Text style={styles.cardTitle}>Location Planning</Text>
  </TouchableOpacity>
</View>
<View style={styles.cardContainer}>
  <TouchableOpacity onPress={() => navigation.navigate("Routecommonscreen")} style={styles.card}>
    <Image source={location} style={styles.image} />
    <Text style={styles.cardTitle}>Route Planning</Text>
  </TouchableOpacity>
</View>
</View>

<View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
<View style={styles.cardContainer}>
  <TouchableOpacity onPress={() => navigation.navigate("HotelWelcomeScreen")} style={styles.card}>
    <Image source={hotel} style={styles.image} />
    <Text style={styles.cardTitle}>Hotel Planning</Text>
  </TouchableOpacity>
</View>
<View style={styles.cardContainer}>
  <TouchableOpacity  style={styles.card}>
    <Image source={carni} style={styles.image} />
    <Text style={styles.cardTitle}>Activities Planning</Text>
  </TouchableOpacity>
</View>
</View>


     
     
<View style={styles.topcard}>
<Text style={styles.Top}>Top Destinations</Text>
<ScrollView horizontal={true}>
  <View style={styles.card}>   
      <Image source={{ uri: 'https://i.postimg.cc/2jPCWYpZ/1.jpg' }} style={styles.topimage} />     
  </View>
  <View style={styles.card}>      
      <Image source={{ uri: 'https://i.postimg.cc/4dLrs6Gc/3.jpg' }} style={styles.topimage} />     
  </View>
  <View style={styles.card}>    
      <Image source={{ uri: 'https://i.postimg.cc/sXKx9Nrh/6.jpg' }} style={styles.topimage} />   
  </View>
  <View style={styles.card}>      
      <Image source={{ uri: 'https://i.postimg.cc/509N0SCN/2.jpg' }} style={styles.topimage} />  
  </View>
</ScrollView>
</View>


</View> 
      </ScrollView>
      <NavigationBar isUsersDetailsActive={true} />
  </SafeAreaView>
 

 
);
};
const styles = StyleSheet.create({
redCard: {
  backgroundColor: '#e6eeff',
  borderTopLeftRadius: 20, 
  borderTopRightRadius: 20, 
  height:'150%',
  width: '100%',

},
profileImage: {
  width: 80,
  height: 80,
  resizeMode: "cover",
  borderRadius: 50,
  alignSelf: "center",
  marginTop: 30,
  marginBottom: 10,
},
header: {
  flexDirection: "row",
  justifyContent: "space-between",
  paddingHorizontal: 16,
  alignItems: "center",
},
horizontalLines: {
  width: 30,
  height: 3,
  backgroundColor: "#fff", 
},
whiteCard: {
  backgroundColor: 'rgba(255, 255, 255, 0.7)',
  borderRadius: 10,
  padding: 20,
  marginBottom: 20,
  marginTop:20,
 
},
profileButton: {
  width: 30,
  height: 30,
  borderRadius: 15,
  overflow: "hidden",
},
avatar: {
  width: "100%",
  height: "100%",
  resizeMode: "cover",
},
heroImageContainer: {

  marginTop: -50, 
},
heroImage: {
  width: Dimensions.get("window").width,
  height: 300,
  resizeMode: "cover",
  justifyContent: "flex-end",
  overflow: "hidden",
  borderBottomRightRadius:35,
  borderBottomLeftRadius: 35,
  shadowColor: 'black',  
  shadowOffset: { width: 0, height: 2 }, 
  shadowOpacity: 0.2,     
  shadowRadius: 6,       
},
avatarContainer: {
  height: 0,
  weidth: 0,
  alignItems: "center",
  marginTop: -220,
  marginLeft:130,
  marginRight:130,
  alignItems: "center",
},
searchBarContainer: {
  paddingHorizontal: 16,
  marginTop: 132,
  paddingBottom: 20,
},
searchBar: {
  height: 50,
  borderColor: "black",
  backgroundColor: "#fff",
  borderWidth: 1,
  borderRadius: 5,
  color: "black",
  paddingHorizontal: 16,
  shadowColor: "#000", 
shadowOffset: {
  width: 0,
  height: 2,
},
shadowOpacity: 0.25,
shadowRadius: 3.84,
flexDirection: "row", 
alignItems: "center", 
},
searchIcon: {
  marginRight: 10,
},
searchText: {
  color: "black", 
  fontSize: 16, 
},
cardContainer: {
  backgroundColor: "#fff",
  borderRadius: 8,
  shadowColor: "#000",
  shadowOpacity: 0.3,
  shadowOffset: { width: 0, height: 2 },
  shadowRadius: 4,
  padding: 10,
  justifyContent: "center",
  alignItems: "center",
  width: Dimensions.get("window").width * 0.45, // Adjust the width as needed
},
topcard: {
  backgroundColor: "#fff",
  borderRadius: 8,
  shadowColor: "#000",
  shadowOpacity: 0.3,
  shadowOffset: { width: 0, height: 2 },
  shadowRadius: 4,
  width: 350,
 marginLeft:15,
 marginRight:25,
 marginTop:24,
 marginBottom:20,
  alignItems: "center",
  
},
card: {
  justifyContent: "center",
  alignItems: "center",
},
image: {
  width: 120,
  height: 120,
  borderRadius: 8,
},
topimage: {
  width: 120,
  height: 120,
  marginLeft:10,
  marginBottom:10,
  borderRadius: 8,
},
cardTitle: {
  fontSize: 18,
  fontWeight: "bold",
  marginTop: 10,
},
welcomeText: {
  fontSize: 30,
  marginTop: 75,
  fontWeight: 'bold',
  textAlign: 'center',
  marginBottom: 20,
  color: 'white',
},
subText: {
  fontSize: 16,
  textAlign: 'center',
  color: 'white',
},
Top: {
  fontSize: 16,
  fontWeight: 'bold',
  textAlign: 'left',
  color: 'black',
  marginLeft: 15,
  justifyContent: 'center',
  alignItems: 'center',
},

overlay: {
  ...StyleSheet.absoluteFillObject,
  justifyContent: 'center',
  alignItems: 'center',
},
overlayText: {
  color: 'white',
  fontSize: 20,
  textAlign: 'center',
  marginVertical: 10,
},
});
export default MainScreen;