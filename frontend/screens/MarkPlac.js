import React, { useState, useRef, useLayoutEffect } from 'react';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { GooglePlaceDetail, GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Constants from 'expo-constants';
import { useNavigation } from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GOOGLE_API_KEY} from "../environments";

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INITIAL_POSITION = {
  latitude: 6.02662175,
  longitude: 80.21768858,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

function InputAutocomplete({
  label,
  placeholder,
  onPlaceSelected,
}) {
  return (
    <>
      <Text>{label}</Text>
      <GooglePlacesAutocomplete
        styles={{ textInput: styles.input }}
        placeholder={placeholder || ''}
        fetchDetails
        onPress={(data, details = null) => {
          onPlaceSelected(details);
        }}
        query={{
          key: GOOGLE_API_KEY,
          language: 'pt-BR',
        }}
      />
    </>
  );
}

export default function MarkPlac() {
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  const [origin, setOrigin] = useState(null);
  const [destination, setDestination] = useState(null);
  const mapRef = useRef(null);
  
  const [destinationLatitude, setDestinationLatitude] = useState(null);
  const [destinationLongitude, setDestinationLongitude] = useState(null);
  const [originLatitude, setOriginLatitude] = useState(null);
  const [originLongitude, setOriginLongitude] = useState(null);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const setData = async () => {
    try {
      setLoading(true);
      const temprating = await AsyncStorage.getItem('temprating');
      const herrating = await AsyncStorage.getItem('herrating');
      const bchrating = await AsyncStorage.getItem('bchrating');
      const parksrating = await AsyncStorage.getItem('parksrating');
      const artrating = await AsyncStorage.getItem('artrating');

      console.log('temprating:', temprating);
      console.log('herrating:', herrating);
      console.log('bchrating:', bchrating);
      console.log('parksrating', parksrating);
      console.log('artrating', artrating);

      const templeValue = parseInt(temprating);
      const heriValue = parseInt(herrating);
      const beachValue = parseInt(bchrating);
      const parkValue = parseInt(parksrating);
      const artValue = parseInt(artrating);

      const newPlace = {
        Username: 'hirushan',
        StartLat: originLatitude,
        StartLon: originLongitude,
        EndLat: destinationLatitude,
        EndLon: destinationLongitude,
        Temples: templeValue,
        Heritages: heriValue,
        Beaches: beachValue,
        Parks : parkValue,
        Arts : artValue
      };

      console.log(newPlace);

      const response = await axios.post(`http://16.171.19.240:5000/api`, newPlace);
      console.log(response.data);

      if (response.status === 200) {
        alert(` You have created: ${JSON.stringify(response.data)}`);
        navigation.navigate('NewTest');
      } else {
        throw new Error('An error has occurred');
      }
    } catch (error) {
      console.log('Error while fetching data from AsyncStorage:', error);
    } finally {
      setLoading(false);
    }
  };

  const moveTo = async (position) => {
    const camera = await mapRef.current?.getCamera();
    if (camera) {
      camera.center = position;
      mapRef.current?.animateCamera(camera, { duration: 1000 });
    }
  };

  const onPlaceSelected = (details, flag) => {
    const set = flag === 'origin' ? setOrigin : setDestination;
    const position = {
      latitude: details?.geometry.location.lat || 0,
      longitude: details?.geometry.location.lng || 0,
    };
    set(position);
    moveTo(position);
    console.log(
      flag === 'origin' ? 'Origin' : 'Destination',
      'Latitude:',
      position.latitude,
      'Longitude:',
      position.longitude
    );
    if (flag === 'origin') {
      setOriginLatitude(position.latitude);
      setOriginLongitude(position.longitude);
    }
    if (flag === 'destination') {
      setDestinationLatitude(position.latitude);
      setDestinationLongitude(position.longitude);
    }
  };
  
  return (
    <View style={styles.container}>
      <MapView ref={mapRef} style={styles.map} provider={PROVIDER_GOOGLE} initialRegion={INITIAL_POSITION}>
        {origin && <Marker coordinate={origin} />}
        {destination && <Marker coordinate={destination} />}
      </MapView>
      <View style={styles.searchContainer}>
        <InputAutocomplete
          label="Origin"
          onPlaceSelected={(details) => {
            onPlaceSelected(details, 'origin');
          }}
        />
        <InputAutocomplete
          label="Destination"
          onPlaceSelected={(details) => {
            onPlaceSelected(details, 'destination');
          }}
        />
        <TouchableOpacity style={styles.button} onPress={setData}>
          <Text style={styles.buttonText}>Confirm</Text>
        </TouchableOpacity>
      </View>
      {loading && (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color="#3498db" />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  searchContainer: {
    position: 'absolute',
    width: '100%',
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
    padding: 8,
    borderRadius: 8,
    top: Constants.statusBarHeight,
  },
  input: {
    borderColor: '#888',
    borderWidth: 1,
  },
  button: {
    marginTop: 16,
    backgroundColor: '#3498db',
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 8,
    alignSelf: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  loadingContainer: {
    position: 'absolute',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
