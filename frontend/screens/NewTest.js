import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { View, Text, Button, Image } from 'react-native';
import MapView, { Marker  } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { GOOGLE_API_KEY } from '../environments';
import { useNavigation } from '@react-navigation/native';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import axios from 'axios';

// Custom component to display location number on top of the marker
const CustomMarker = ({ coordinate, number,  title}) => (
  <Marker 
  coordinate={coordinate}
  title={title[number-2]}>
    <View style={{ backgroundColor: 'red', padding: 5, borderRadius: 10 }}>
      <Text style={{ color: 'white', fontWeight: 'bold' }}>{number - 1}</Text>
    </View>
    {/* <Callout>
      <View style={{ backgroundColor: 'white', padding: 10, borderRadius: 5 }}>
        <Text style={{ color: 'black' }}>hiii</Text>
      </View>
    </Callout> */}
  </Marker>
);

const NewTest = () => {
  const navigation = useNavigation();
  const [point, setPoint] = useState([]);
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [watchingLocation, setWatchingLocation] = useState(false);
  const [places, setPlaces] = useState([]);
  const [routeDistance, setRouteDistance] = useState(0);
  const [routeDuration, setRouteDuration] = useState({ hours: 0, minutes: 0 });
  
    useLayoutEffect(() => {
      navigation.setOptions({
        headerShown: false,
      });
    }, []);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        console.log('Permission to access location was denied');
        return;
      }

      // Get the device's initial location
      try {
        const initialLocation = await Location.getCurrentPositionAsync({});
        setLocation(initialLocation);
      } catch (error) {
        setErrorMsg('Error getting location');
      }

      function getLocations() {
        axios
          .get('http://16.171.19.240:5000/lastrecodee')
          .then((res) => {
            setPlaces(res.data.routesflask.result);
            console.log('res', res.data.routesflask.result);
          })
          .catch((err) => {
            alert(err.msg);
          });
      }
      getLocations();

      function getPlaces() {
        axios
          .get('http://16.171.19.240:5000/lastlocation')
          .then((res) => {
            setPoint(res.data.locationsflask.places);
            console.log('ressssss', res.data.locationsflask.places);
          })
          .catch((err) => {
            alert(err.msg);
          });
      }
      getPlaces();
    })();
  }, []);

  useEffect(() => {
    let locationSubscription;

    // Start watching location updates when the user presses the Start button
    console.log('kkkkkkkkkkkkkk');
    console.log(watchingLocation);
    if (watchingLocation) {
      console.log(watchingLocation);
      console.log('ggggggggggggggg');
      locationSubscription = Location.watchPositionAsync(
        {
          accuracy: Location.Accuracy.High,
          timeInterval: 5000, // Update every 5 seconds
          distanceInterval: 10, // Update every 10 meters
        },
        (location) => {
          setLocation(location);
        }
      );
    } else {
      // Stop watching location updates when the user presses the Stop button
      console.log("fffffffffffffffffffffffffffffffffffff");
      console.log(watchingLocation);
      if (locationSubscription) {
        locationSubscription.remove;
        console.log("vvvvvvvvvvvvvvvvv");
      }
    }

    return () => {
      // Clean up the location subscription when the component is unmounted
      if (locationSubscription) {
        locationSubscription.remove;
      }
    };
  }, [watchingLocation]);

  // Function to toggle location tracking
  const handleStartStop = () => {
    console.log('hiiiiiiiii')
    console.log(watchingLocation);
    setWatchingLocation((prev) => !prev);
    console.log(watchingLocation);
  };

  // Function to update the user's location on the map
  const updateUserLocation = () => {
    if (location) {
      // Get the user's current latitude and longitude
      const latitude = location.coords.latitude;
      const longitude = location.coords.longitude;

      // Set the new region to focus on the user's location
      const newRegion = {
        latitude,
        longitude,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      };

      // Update the map's region to focus on the user's location
      mapViewRef.current.animateToRegion(newRegion, 1000);

      // Return a custom marker for the user's current location
      return (
        <Marker coordinate={{ latitude, longitude }}>
          <Image source={require('../assets/avatar.png')} style={{ width: 20, height: 20 }} />
        </Marker>
      );
    }

    // Return null if the location is not available yet
    return null;
  };

  const mapViewRef = useRef(null);

  // Function to update the direction and distance information between user's location and the first location
  const updateDirectionInfo = async () => {
    if (places.length > 0) {
      let totalDistance = 0;
      let totalDuration = 0;

      for (let i = 0; i < places.length - 2; i += 2) {
        const origin = `${places[i]},${places[i + 1]}`;
        const destination = `${places[i + 2]},${places[i + 3]}`;
        const directionApiUrl = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${GOOGLE_API_KEY}`;

        try {
          const response = await axios.get(directionApiUrl);
          const { routes } = response.data;
          if (routes && routes.length > 0) {
            const route = routes[0];
            totalDistance += route.legs[0].distance.value;
            totalDuration += route.legs[0].duration.value;
          }
        } catch (error) {
          console.error('Error fetching direction:', error);
        }
      }

      // Convert total distance from meters to km or miles based on your preference
      // For example, if you want the distance in kilometers:
      const totalDistanceInKm = totalDistance / 1000;
      setRouteDistance(totalDistanceInKm);

      // Convert total duration from seconds to hours and minutes
      const totalDurationInMinutes = totalDuration / 60;
      const hours = Math.floor(totalDurationInMinutes / 60);
      const minutes = Math.round(totalDurationInMinutes % 60);
      setRouteDuration({ hours, minutes });
    }
  };

  useEffect(() => {
    // Update direction info when location or places array changes
    updateDirectionInfo();
  }, [places]);

  return (
    <View style={{ flex: 1 }}>
      <MapView
        ref={mapViewRef}
        style={{ flex: 1 }}
        initialRegion={{
          latitude: places.length > 0 ? places[0] : 0,
          longitude: places.length > 0 ? places[1] : 0,
          latitudeDelta: 0.5,
          longitudeDelta: 0.5,
        }}
      >
        {places.map((place, index) => {
          if (index % 2 !== 0) {
            // Skip if the index is odd since it represents longitude
            return null;
          }

          const latitude = place;
          const longitude = places[index + 1];

          if (index === 0) {
            // Use default marker for the first location (index 0)
            return (
              <Marker
                key={index}
                coordinate={{
                  latitude,
                  longitude,
                }}
                title={`Location ${index / 2 + 1}`}
                description={`Latitude: ${latitude}, Longitude: ${longitude}`}
                pinColor="red" // Set the color of the first marker to red
              />
            );
          } else if (index === places.length - 2) {
            // Use default marker for the last location (index = locations.length - 2)
            return (
              <Marker
                key={index}
                coordinate={{
                  latitude,
                  longitude,
                }}
                title={`Location ${places.length / 2}`}
                description={`Latitude: ${latitude}, Longitude: ${longitude}`}
                pinColor="blue" // Set the color of the last marker to blue
              />
            );
          } else {
            // Use custom marker for other locations
            return (
              <CustomMarker
                key={index}
                coordinate={{
                  latitude,
                  longitude,
                }}
                number={index / 2 + 1} // Calculate location number
                title = {point}
              />
            );
          }
        })}

        {/* Draw directions from user's current location to the first location */}
        {location && places.length > 0 && (
          <MapViewDirections
            origin={location.coords}
            destination={{
              latitude: places[0],
              longitude: places[1],
            }}
            apikey={GOOGLE_API_KEY}
            strokeWidth={3}
            strokeColor="blue" // Set the color of the first route to green
          />
        )}

        {/* Draw directions between consecutive locations */}
        {places.map((place, index) => {
          if (index % 2 !== 0 || index >= places.length - 2) {
            // Skip if the index is odd or if we have reached the last location
            return null;
          }

          const origin = { latitude: places[index], longitude: places[index + 1] };
          const destination = {
            latitude: places[index + 2],
            longitude: places[index + 3],
          };

          return (
            <MapViewDirections
              key={index}
              origin={origin}
              destination={destination}
              apikey={GOOGLE_API_KEY}
              strokeWidth={3}
              strokeColor="blue"
            />
          );
        })}

        {/* Show the user's current location on the map */}
        {updateUserLocation()}
      </MapView>

      {/* Button to start/stop location tracking */}
      <Button title={watchingLocation ? 'Stop' : 'Start'} onPress={handleStartStop} />

      {/* Display total route distance and estimated time */}
      <View style={{ alignSelf: 'center', marginTop: 10 }}>
        <Text style={{ fontWeight: 'bold' }}>Total Route Distance: {routeDistance} km</Text>
        <Text style={{ fontWeight: 'bold', marginBottom:10 }}>
          Estimated Time: {routeDuration.hours}h {routeDuration.minutes}min
        </Text>
      </View>
    </View>
  );
};

export default NewTest;
