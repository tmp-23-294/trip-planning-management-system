import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import { View, Text, Button } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { GOOGLE_API_KEY } from '../environments';
import { useNavigation } from '@react-navigation/native';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import axios from 'axios';

// Custom component to display location number on top of the marker
const CustomMarker = ({ coordinate, number }) => (
  <Marker coordinate={coordinate}>
    <View style={{ backgroundColor: 'red', padding: 5, borderRadius: 10 }}>
      <Text style={{ color: 'white', fontWeight: 'bold' }}>{number}</Text>
    </View>
  </Marker>
);

const Newfinal = () => {
  const navigation = useNavigation();
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [watchingLocation, setWatchingLocation] = useState(false);
  const [places, setPlaces] = useState([]);

  useEffect(() => {
    (async () => {
      // Request location permissions
      const { status } = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      // Get the device's initial location
      try {
        const initialLocation = await Location.getCurrentPositionAsync({});
        setLocation(initialLocation);
      } catch (error) {
        setErrorMsg('Error getting location');
      }

      function getLocations() {
        axios
          .get("http://13.51.86.151:5000/lastrecode")
          .then((res) => {
            setPlaces(res.data.routesflask.result);
            console.log('res', res.data.routesflask.result);
          })
          .catch((err) => {
            alert(err.msg);
          });
      }
      getLocations();
    })();
  }, []);

  useEffect(() => {
    let locationSubscription;

    // Start watching location updates when the user presses the Start button
    if (watchingLocation) {
      locationSubscription = Location.watchPositionAsync(
        {
          accuracy: Location.Accuracy.High,
          timeInterval: 5000, // Update every 5 seconds
          distanceInterval: 10, // Update every 10 meters
        },
        (location) => {
          setLocation(location);
        }
      );
    } else {
      // Stop watching location updates when the user presses the Stop button
      if (locationSubscription) {
        locationSubscription.remove();
      }
    }

    return () => {
      // Clean up the location subscription when the component is unmounted
      if (locationSubscription) {
        locationSubscription.remove();
      }
    };
  }, [watchingLocation]);

  // Function to toggle location tracking
  const handleStartStop = () => {
    setWatchingLocation((prev) => !prev);
  };

  // Function to update the user's location on the map
  const updateUserLocation = () => {
    if (location) {
      // Get the user's current latitude and longitude
      const latitude = location.coords.latitude;
      const longitude = location.coords.longitude;

      // Set the new region to focus on the user's location
      const newRegion = {
        latitude,
        longitude,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      };

      // Update the map's region to focus on the user's location
      mapViewRef.current.animateToRegion(newRegion, 1000);

      // Return a custom marker for the user's current location
      return (
        <Marker coordinate={{ latitude, longitude }}>
          <View style={{ backgroundColor: 'blue', padding: 5, borderRadius: 10 }}>
            <Text style={{ color: 'white', fontWeight: 'bold' }}>You are here</Text>
          </View>
        </Marker>
      );
    }

    // Return null if the location is not available yet
    return null;
  };

  const mapViewRef = useRef(null);

  return (
    <View style={{ flex: 1 }}>
      <MapView
        ref={mapViewRef}
        style={{ flex: 1 }}
        initialRegion={{
          latitude: places.length > 0 ? places[0].latitude : 0,
          longitude: places.length > 0 ? places[0].longitude : 0,
          latitudeDelta: 0.5,
          longitudeDelta: 0.5,
        }}
      >
        {places.map((place, index) => {
          const { latitude, longitude } = place;

          return (
            <CustomMarker
              key={index}
              coordinate={{ latitude, longitude }}
              number={index + 1} // Display location number starting from 1
            />
          );
        })}

        {/* Draw directions between consecutive locations */}
        {places.map((place, index) => {
          if (index >= places.length - 1) {
            // Skip if we have reached the last location
            return null;
          }

          const origin = places[index];
          const destination = places[index + 1];

          return (
            <MapViewDirections
              key={index}
              origin={origin}
              destination={destination}
              apikey={GOOGLE_API_KEY}
              strokeWidth={3}
              strokeColor="blue"
            />
          );
        })}

        {/* Show the user's current location on the map */}
        {updateUserLocation()}
      </MapView>

      {/* Button to start/stop location tracking */}
      <Button title={watchingLocation ? 'Stop' : 'Start'} onPress={handleStartStop} />
    </View>
  );
};

export default Newfinal;
