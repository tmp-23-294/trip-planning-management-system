import React, { useState, useEffect } from 'react';
import { Button, Image, View, ActivityIndicator, Text } from 'react-native';
import * as ImagePicker from 'expo-image-picker';

import { Amplify, Storage } from 'aws-amplify';
import awsconfig from '../src/aws-exports';
Amplify.configure(awsconfig);

import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from "@react-navigation/native";

export default function Profilepicture() {
  const navigation = useNavigation();
  const [image, setImage] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);

  useEffect(() => {
    if (uploadProgress === 100) {
      setIsLoading(false); // Reset loading state after upload is complete
    }
  }, [uploadProgress]);

  const fetchImageUrI = async (uri) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    return blob;
  };

  const uploadfile = async (file) => {
    setIsLoading(true);
    const username = await AsyncStorage.getItem('username');
    const img = await fetchImageUrI(file);

    return Storage.put(`profPic/${username}/${username}.jpg`, img, {
      level: 'public',
      contentType: file.type,
      progressCallback(uploadProgress) {
        setUploadProgress(Math.round((uploadProgress.loaded / uploadProgress.total) * 100));
      },
    })
      .then((res) => {
        Storage.get(res.key)
          .then((result) => {
            console.log('Result--', result);
            navigation.navigate('Login');
          })
          .catch((e) => {
            console.log(e);
          });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const onSavePress = async () => {
    if (image) {
      await uploadfile(image);
      // You can add any additional logic or actions here after the image is saved.
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
    });

    console.log(result);
    if (!result.canceled) {
      setImage(result.uri);
    }
  };

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Upload profile picture" onPress={pickImage} />
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
      {image && (
        <View>
          <Button title="Save" onPress={onSavePress} />
          {isLoading && <ActivityIndicator size="small" color="#0000ff" />}
          {uploadProgress > 0 && <Text>{`Uploading: ${uploadProgress}%`}</Text>}
        </View>
      )}
    </View>
  );
}
