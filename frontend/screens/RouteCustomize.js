import {
    View,
    Text,
    SafeAreaView,
    Image,
    TextInput,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
  } from "react-native";
  import React, { useEffect, useLayoutEffect, useState } from "react";
  import * as Animatable from "react-native-animatable";
  import { useNavigation } from "@react-navigation/native";
  import { Avatar } from "../assets";
  import { location } from "../assets";
  import { RouteIm } from "../assets";
  import Icon from 'react-native-vector-icons/FontAwesome';

const RouteCustomize = () => {
  const navigation = useNavigation();
  const [inputFields, setInputFields] = useState(1);

    useLayoutEffect(() => {
      navigation.setOptions({
        headerShown: false,
      });
    }, []);

    const handlePlusPress = () => {
      setInputFields(inputFields + 1);
    };

    const handleMinusPress = () => {
      setInputFields(inputFields - 1);
    };
  return (
    <SafeAreaView className="flex-1 bg-emerald-100 relative">
      <View className="flex-row items-center justify-between px-8">
        <View>
          <Text className="text-[40px] text-[#0B646B] font-bold">Discover</Text>
          <Text className="text-[#527283] text-[36px]">the beauty today</Text>
        </View>

        <View className="w-12 h-12 bg-gray-400 rounded-md items-center justify-center shadow-lg">
          <Image
            source={Avatar}
            className="w-full h-full rounded-md object-cover"
          />
        </View>
      </View>
      <View className="w-full bg-white dark:bg-gray-50 rounded-3xl p-10 top-5">

      <View>
        <Text className="text-[20px] text-[#0B646B] font-bold">Enter Your Email </Text>
        <View className = "bg-zinc-200 rounded-full" >
          <TextInput placeholder="Email" />
          </View>
        
        </View>

        <View>
        <Text className="text-[20px] text-[#0B646B] font-bold -bottom-2">Starting Location</Text>
        <View className="flex flex-1 ">
        <TouchableOpacity className="bg-blue-500 rounded-lg p-3 -bottom-2 w-60 h-12"
        onPress={() => navigation.navigate("RouteDisplay")}>
          <Text className="text-white">Set Your Starting Location</Text>

        </TouchableOpacity>

      </View>
        </View>
        <View>
        <Text className="text-[20px] text-[#0B646B] font-bold -bottom-4">Destination Location</Text>
        <View className="flex flex-1 ">
        <TouchableOpacity className="bg-blue-500 rounded-lg p-3 -bottom-4 w-60 h-12"
        onPress={() => navigation.navigate("RouteDisplay")}>
          <Text className="text-white">Set Your Destination Location</Text>

        </TouchableOpacity>

      </View>
        </View>

        <View>
        <Text className="text-[20px] text-[#0B646B] font-bold -bottom-6">Enter Your Prefered Locations </Text>
      {/* Render the desired number of input fields */}
      {Array.from({ length: inputFields }).map((_, index) => (
        <TextInput
          key={index}
          placeholder="Input Field"
          className ="-bottom-6 bg-zinc-200 rounded-full mt-5"
        />
      ))}

      {/* Render the plus icon */}
      <TouchableOpacity className = " mt-10"onPress={handlePlusPress}>
      <Icon name="plus" size={24} color="black" />
      </TouchableOpacity>
    </View>
    <View>
              {/* Render the plus icon */}
      <TouchableOpacity onPress={handleMinusPress} className="ml-10 bottom-6">
      <Icon name="minus" size={24} color="black" />
      </TouchableOpacity>
    </View>
    <View className="flex flex-1">
        <TouchableOpacity className="bg-blue-500 rounded-lg p-3  w-20 h-12 ml-64 bottom-10"
        onPress={() => navigation.navigate("RouteDisplay")}>
          <Text className="text-white">Submit</Text>

        </TouchableOpacity>

      </View>
      </View>
    </SafeAreaView>
   
  );
};

export default RouteCustomize;