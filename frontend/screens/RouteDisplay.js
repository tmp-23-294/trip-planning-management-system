import React, {useState, useEffect} from 'react';
import MapView, {Marker} from 'react-native-maps';
import { StyleSheet, View, Text, Dimensions, Button } from 'react-native';
import * as Location from 'expo-location';

export default function RouteDisplay() {
    const [mapRegion, setMapRegion] = useState({
        latitude: 37.78825,
        longitude: -122.78825,
        latitudeDelta: 0.0922,
        longitudeDelta:0.0421,
    });

const userLocation = async () => {
    let {status} = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted'){
        setErrorMsg('Permission to access loaction was denied');
    }
    let loaction = await Location.getCurrentPositionAsync({enableHighAccuracy: true});
    setMapRegion({
        latitude: loaction.coords.latitude,
        longitude: loaction.coords.longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    });
    console.log(loaction.coords.latitude, loaction.coords.longitude);
}
useEffect(() => {
    userLocation();
}, []);
  return (
    <View style={styles.container}>
      <MapView style={styles.map} 
       region={mapRegion}
       >
        <Marker coordinate={mapRegion} title='Marker'/>
     </MapView>
     <Button title='Get Location' onPress={userLocation}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '100%',
  },
});
