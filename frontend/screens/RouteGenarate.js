import {
  View,
  Text,
  SafeAreaView,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import React, { useEffect, useLayoutEffect, useState } from "react";
import * as Animatable from "react-native-animatable";
import { useNavigation } from "@react-navigation/native";
import { Avatar } from "../assets";
import { location } from "../assets";
import { RouteIm } from "../assets";
import Icon from 'react-native-vector-icons/FontAwesome';
import DropDownPicker from 'react-native-dropdown-picker';

const RouteGenarate = () => {
const navigation = useNavigation();
const [inputFields, setInputFields] = useState(1);
//const [selectedValue, setSelectedValue] = useState(0);
const [open, setOpen] = useState(false);
const [value, setValue] = useState(null);
const [items, setItems] = useState([
  {label: '0', value: '0'},
  {label: '1', value: '1'},
  {label: '2', value: '2'},
  {label: '3', value: '3'},
  {label: '4', value: '4'},
  {label: '5', value: '5'},

]);
const [openn, setOpenn] = useState(false);
const [valuee, setValuee] = useState(null);
const [itemss, setItemss] = useState([
  {label: '0', value: '0'},
  {label: '1', value: '1'},
  {label: '2', value: '2'},
  {label: '3', value: '3'},
  {label: '4', value: '4'},
  {label: '5', value: '5'},

]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

return (
  <SafeAreaView className="flex-1 bg-emerald-100 relative">
    <View className="flex-row items-center justify-between px-8">
      <View>
        <Text className="text-[40px] text-[#0B646B] font-bold">Discover</Text>
        <Text className="text-[#527283] text-[36px]">the beauty today</Text>
      </View>

      <View className="w-12 h-12 bg-gray-400 rounded-md items-center justify-center shadow-lg">
        <Image
          source={Avatar}
          className="w-full h-full rounded-md object-cover"
        />
      </View>
    </View>
    <View className="w-full bg-white dark:bg-gray-50 rounded-3xl p-10 top-5">

    <View>
      <Text className="text-[20px] text-[#0B646B] font-bold">Enter Your Email </Text>
      <View className = "bg-zinc-200 rounded-full" >
        <TextInput placeholder="Email" />
        </View>
      
      </View>

      <View>
      <Text className="text-[20px] text-[#0B646B] font-bold -bottom-2">Starting Location</Text>
      <View className="flex flex-1 ">
      <TouchableOpacity className="bg-blue-500 rounded-lg p-3 -bottom-2 w-60 h-12"
      onPress={() => navigation.navigate("RouteDisplay")}>
        <Text className="text-white">Set Your Starting Location</Text>

      </TouchableOpacity>

    </View>
      </View>
      <View>
      <Text className="text-[20px] text-[#0B646B] font-bold -bottom-4">Destination Location</Text>
      <View className="flex flex-1 ">
      <TouchableOpacity className="bg-blue-500 rounded-lg p-3 -bottom-4 w-60 h-12"
      onPress={() => navigation.navigate("RouteDisplay")}>
        <Text className="text-white">Set Your Destination Location</Text>

      </TouchableOpacity>

    </View>
      </View>
      {/* <View>
        <Text className="text-[20px] text-[#0B646B] font-bold -bottom-6">Enter Your Rarings </Text>
      </View> */}

      <View>
      <DropDownPicker
      className = "-bottom-10"
      open={open}
      value={value}
      items={items}
      setOpen={setOpen}
      setValue={setValue}
      setItems={setItems}
    />
</View>
<View>
      <DropDownPicker
      className = "-bottom-20"
      open={openn}
      value={valuee}
      items={itemss}
      setOpen={setOpenn}
      setValue={setValuee}
      setItems={setItemss}
    />
</View>
    </View>
  </SafeAreaView>
 
);
};


export default RouteGenarate;