import {
  View,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  StyleSheet,
  Dimensions
} from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage';
import NavigationBar from '../components/NavigationBar';
import React, { useEffect, useLayoutEffect, useState } from "react";
import * as Animatable from "react-native-animatable";
import { useNavigation } from "@react-navigation/native";
import { sdsfddd } from "../assets";
import { location } from "../assets";
import { RouteIm } from "../assets";
import { routehero } from "../assets";
import RouteGenarate from "./RouteGenarate";

import { gggggg } from "../assets";
import { hhhhhh } from "../assets";

const RouteHomePage = () => {
  const [templerating, setTempleRrating] = useState(null);
  const [heritagesrating, setHeritagesrating] = useState(null);
  const [beachesrating, setBeachesrating] = useState(null);
  const navigation = useNavigation();
  const [parksrating, setParksrating] = useState(null);
  const [artrating, setArtrating] = useState(null);

  const handleRatingChange = (text) => {
    const parsedValue = parseInt(text);
    if (isNaN(parsedValue) || parsedValue <= 0 || parsedValue >= 10) {
      return;
    }

    setTempleRrating(text);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const save = () =>{
    if (templerating === null || heritagesrating === null || beachesrating === null) {
      Alert.alert('Invalid Input', 'Please enter all the ratings before saving.');
      return;
    }
    else if(templerating >10 || heritagesrating >10 || beachesrating > 10 || templerating < 0 || heritagesrating < 0 || beachesrating < 0){
      Alert.alert('Invalid Input', 'Rating cannot be greater than 10 and cannt be lower than 0');
      return;
    }

  else {
    AsyncStorage.setItem("temprating", templerating);
    AsyncStorage.setItem("herrating", heritagesrating);
    AsyncStorage.setItem("bchrating", beachesrating);
    AsyncStorage.setItem("parksrating", parksrating);
    AsyncStorage.setItem("artrating", artrating);
  
    console.log(templerating);
    console.log(heritagesrating);
    console.log(beachesrating);
    console.log(parksrating);
    console.log(artrating)

    setTempleRrating(null);
    setHeritagesrating(null);
    setBeachesrating(null);
    setParksrating(null);
    setArtrating(null);

    navigation.navigate('MarkPlac');

  }
    // All ratings are valid, proceed to save
    

  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
    
      
 

    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
            <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
          </View>
        <View style={styles.container}>
        <Text style={styles.welcomeText}>Welcome to Route Home!</Text>
          <Text style={styles.subText}>Explore, Plan, and Enjoy Your Journey</Text>
        <ScrollView>
<View >
      <View style={styles.card}>

          <Text style={{fontSize: 20, marginBottom:15, fontWeight: 'bold'}}>Auto Route generate</Text>
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center", top: 0}}>
              <TouchableOpacity
                style={{ backgroundColor: "#4287f5", borderRadius: 10, padding: 10, marginTop: 10,marginBottom:20, width: 350, height: 40, justifyContent: "center", alignItems: "center", width: 300 }}
                onPress={() => navigation.navigate("NewTest")}
              >
                <Text style={{ color: "#fff", fontSize: 15 }}>Current Route</Text>
              </TouchableOpacity>
            </View>
          <View style={{marginBottom:15}}>
           <View >
              <Image source={RouteIm} style={{width:200, height:200}} />
            </View>
      </View>
      
         
          <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%", marginBottom: 20, flexWrap: "nowrap",}}>
              <Text style={{ fontStyle: "normal", fontWeight: "600", fontSize: 18, lineHeight: 36, paddingVertical: 5,}}>Temple Rating          : </Text>
              <TextInput
                 style={{ width: "40%", height: 40, paddingHorizontal: 10, borderColor: "#ccc", borderWidth: 1, borderRadius: 4, marginTop: 8, }}
                 placeholder="Rating 0-10"
                 keyboardType="numeric"
                 onChangeText={setTempleRrating}
                 value={templerating}
               />
              </View>
              <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%", marginBottom: 10, flexWrap: "nowrap", marginTop: -15}}>
              <Text style={{ fontStyle: "normal", fontWeight: "600", fontSize: 18, lineHeight: 36, paddingVertical: 5,}}>Heritages Rating      : </Text>
              <TextInput
                 style={{ width: "40%", height: 40, paddingHorizontal: 10, borderColor: "#ccc", borderWidth: 1, borderRadius: 4, marginTop: 8, }}
                 placeholder="Rating 0-10"
                 keyboardType="numeric"
                 onChangeText={setHeritagesrating}
                 value={heritagesrating}
               />
              </View>
              <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%", marginBottom: 10, flexWrap: "nowrap", marginTop: -5}}>
              <Text style={{ fontStyle: "normal", fontWeight: "600", fontSize: 18, lineHeight: 36, paddingVertical: 5,}}>Beaches Rating        : </Text>
              <TextInput
                 style={{ width: "40%", height: 40, paddingHorizontal: 10, borderColor: "#ccc", borderWidth: 1, borderRadius: 4, marginTop: 8, }}
                 placeholder="Rating 0-10"
                 keyboardType="numeric"
                 onChangeText={setBeachesrating}
                 value={beachesrating}
               />
              </View>
              <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%", marginBottom: 10, flexWrap: "nowrap", marginTop: -5}}>
              <Text style={{ fontStyle: "normal", fontWeight: "600", fontSize: 18, lineHeight: 36, paddingVertical: 5,}}>Parks Rating             : </Text>
              <TextInput
                 style={{ width: "40%", height: 40, paddingHorizontal: 10, borderColor: "#ccc", borderWidth: 1, borderRadius: 4, marginTop: 8, }}
                 placeholder="Rating 0-10"
                 keyboardType="numeric"
                 onChangeText={setParksrating}
                 value={parksrating}
               />
              </View>
              <View style={{flexDirection: "row", justifyContent: "space-between", width: "100%", marginBottom: 10, flexWrap: "nowrap", marginTop: -5}}>
              <Text style={{ fontStyle: "normal", fontWeight: "600", fontSize: 18, lineHeight: 36, paddingVertical: 5,}}>Art Galleries Rating : </Text>
              <TextInput
                 style={{ width: "40%", height: 40, paddingHorizontal: 10, borderColor: "#ccc", borderWidth: 1, borderRadius: 4, marginTop: 8, }}
                 placeholder="Rating 0-10"
                 keyboardType="numeric"
                 onChangeText={setArtrating}
                 value={artrating}
               />
              </View>
              <View style={{ flex: 1, justifyContent: "center", alignItems: "center", top: 12}}>
              <TouchableOpacity
                style={{ backgroundColor: "#4287f5", borderRadius: 10, padding: 10, marginTop: 10,marginBottom:20, width: 300, height: 40, justifyContent: "center", alignItems: "center" }}
                onPress={() => save()}
              >
                <Text style={{ color: "#fff", fontSize: 15 }}>Set Starting and Destination Location</Text>
              </TouchableOpacity>
            </View>
        </View>

      
        </View>
        </ScrollView>
        <NavigationBar/>
    </View>

</SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop:-300
  },
  card: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    padding: 16,
    marginVertical: 8,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 3,
    width: Dimensions.get("window").width * 0.95,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    marginTop: 16,
    backgroundColor: '#3498db',
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 8,
    alignSelf: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  welcomeText: {
    fontSize: 30,
    marginTop: -145,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  subText: {
    fontSize: 16,
    marginBottom: 60,
    textAlign: 'center',
    color: 'white',
  },
});

export default RouteHomePage;


