import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { Text, 
  ScrollView,
  View, 
  StyleSheet, 
  TouchableOpacity, 
  Image, 
  Platform, 
  FlatList, 
  Button, 
  Dimensions,
  SafeAreaView,
  ActivityIndicator} from 'react-native';
  import { useNavigation } from "@react-navigation/native";
import { gggggg } from "../assets";
import { hhhhhh } from "../assets";
import { Avatar } from "../assets";
import { routehero } from "../assets";
import { RouteIm } from "../assets";
import NavigationBar from '../components/NavigationBar';

  
export default function Routecommonscreen() {
    const navigation = useNavigation();

    useLayoutEffect(() => {
        navigation.setOptions({
          headerShown: false,
        });
      }, []);
  
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
    
      
      {/* <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingHorizontal: 8 }}>
            <View>
              <Text style={{ fontSize: 40, color: "#0B646B", fontWeight: "bold" }}>Discover</Text>
              <Text style={{ color: "#527283", fontSize: 36 }}>the beauty today</Text>
            </View>

            <View style={{ width: 60, height: 60, backgroundColor: "#ccc", borderRadius: 30, alignItems: "center", justifyContent: "center", shadowColor: "#000", shadowOffset: { width: 0, height: 4 }, shadowOpacity: 0.3, shadowRadius: 4, elevation: 5 }}>
              <Image source={Avatar} style={{ width: "100%", height: "100%", borderRadius: 30 }} />
            </View>
          </View> */}

          <View style={{ flex: 1, alignItems: "center", justifyContent: "center" , marginTop: -70,}}>
            <Image source={routehero} style={{ width: "100%", height: 380, resizeMode: "cover", position: "absolute", top: 5,  }} />
          </View>
          <View style={styles.container}>
          <Text style={styles.welcomeText}>Welcome to Route Home!</Text>
          <Text style={styles.subText}>Explore, Plan, and Enjoy Your Journey</Text>
          <ScrollView>
 <View >
        <View style={styles.card1}>

            <Text style={{fontSize: 20, marginBottom:15, fontWeight: 'bold',}}>Auto Route generate</Text>
            <View style={{marginBottom:15}}>
          <Image
            source={gggggg}
            
            style={{width:200, height:200}}
          />
        </View>
            <Text style={{fontSize: 15, textAlign: 'justify'}}>An automated route generation system tailors routes based on user preferences, optimizing travel efficiency and convenience. It considers factors such as preferred modes of transportation, scenic routes, time constraints, and points of interest to create customized journeys that align with individual needs.</Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("RouteHomePage")}>
          <Text style={styles.buttonText}>Route Generate</Text>
        </TouchableOpacity>
          </View>

          <View style={styles.card}>
            <Text style={{fontSize: 20, marginBottom:15, fontWeight: 'bold',}}>Customize Route</Text>
            <View style={{marginBottom:15}}>
          <Image
            source={hhhhhh}
            
            style={{width:200, height:200}}
          />
        </View>
            <Text style={{fontSize: 15,  textAlign: 'justify'}}>Customized routing, often referred to as "custom route planning," involves tailoring navigation paths to specific preferences or requirements. This process goes beyond standard directions by allowing individuals or businesses to define personalized routes based on factors like preferred roads, scenic views, specific waypoints, or avoiding certain areas. Whether for efficient travel, sightseeing, or logistical optimization, customized routing provides a tailored navigation experience that aligns closely with unique needs and preferences.</Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("CustomizeRoute")}>
          <Text style={styles.buttonText}>Customize Route</Text>
        </TouchableOpacity>
          </View>
          </View>
          </ScrollView>
          <NavigationBar title="Card 1" content="This is the content of Card 1." />
      </View>

  </SafeAreaView>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop:-312
  },
  card: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    padding: 16,
    marginVertical: 5,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 3,
    width: Dimensions.get("window").width * 1,
    justifyContent: "center",
    alignItems: "center",
  },
  card1: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    borderTopRightRadius: 60,
    borderTopLeftRadius:60,
    padding: 16,
    marginVertical: 5,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 3,
    width: Dimensions.get("window").width * 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    marginTop: 16,
    backgroundColor: '#3498db',
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 8,
    alignSelf: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  welcomeText: {
    fontSize: 30,
    marginTop: -145,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: 'white',
  },
  subText: {
    fontSize: 16,
    marginBottom: 60,
    textAlign: 'center',
    color: 'white',
  },
  
});