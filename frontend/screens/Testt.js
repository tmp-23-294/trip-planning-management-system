import React, { useState, useRef, useLayoutEffect } from 'react';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { GooglePlaceDetail, GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Constants from 'expo-constants';
import { useNavigation } from '@react-navigation/native';
import MapViewDirections from 'react-native-maps-directions';
import {GOOGLE_API_KEY} from "../environments";

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INITIAL_POSITION = {
  latitude: 6.02662175,
  longitude: 80.21768858,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

function InputAutocomplete({
  label,
  placeholder,
  onPlaceSelected,
}) {
  return (
    <>
      <Text>{label}</Text>
      <GooglePlacesAutocomplete
        styles={{ textInput: styles.input }}
        placeholder={placeholder || ''}
        fetchDetails
        onPress={(data, details = null) => {
          onPlaceSelected(details);
        }}
        query={{
          key: GOOGLE_API_KEY, // Make sure to replace with your actual API key
          language: 'pt-BR',
        }}
      />
    </>
  );
}

export default function Test() {
  const navigation = useNavigation();
  const [origin, setOrigin] = useState(null);
  const [destinations, setDestinations] = useState([]);
  const mapRef = useRef(null);

  const [showDirections, setShowDirections] = useState(false);
  const [totalDistance, setTotalDistance] = useState(0);
  const [totalDuration, setTotalDuration] = useState(0);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const traceRoute = () => {
    if (origin && destinations.length > 0) {
      const waypoints = destinations.map(dest => ({
        latitude: dest.latitude,
        longitude: dest.longitude,
      }));

      setShowDirections(true);

      const allWaypoints = [origin, ...waypoints];

      mapRef.current?.fitToCoordinates(allWaypoints, { edgePadding: styles.edgePadding });
    }
  };

  const traceRouteOnReady = (args) => {
    if (args) {
      const totalDistance = args.distance;
      const totalDuration = args.duration;
      setTotalDistance(totalDistance);
      setTotalDuration(totalDuration);
    }
  };

  const moveTo = async (position) => {
    const camera = await mapRef.current?.getCamera();
    if (camera) {
      camera.center = position;
      mapRef.current?.animateCamera(camera, { duration: 1000 });
    }
  };

  const onPlaceSelected = (details, flag) => {
    const set = flag === 'origin' ? setOrigin : (flag === 'destination' ? setDestinations : null);

    if (set) {
      const position = {
        latitude: details?.geometry.location.lat || 0,
        longitude: details?.geometry.location.lng || 0,
      };
      set((prev) => [...prev, position]);
      moveTo(position);
    }
  };

  return (
    <View style={styles.container}>
      <MapView
        ref={mapRef}
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        initialRegion={INITIAL_POSITION}
      >
        {origin && <Marker coordinate={origin} />}
        {destinations.map((dest, index) => (
          <Marker key={index} coordinate={dest} pinColor="blue" />
        ))}
        {showDirections && origin && destinations.length > 0 && (
          <MapViewDirections
            origin={origin}
            waypoints={destinations}
            destination={destinations[destinations.length - 1]}
            apikey={GOOGLE_API_KEY} // Make sure to replace with your actual API key
            strokeColor="#6644ff"
            strokeWidth={4}
            onReady={traceRouteOnReady}
          />
        )}
      </MapView>
      <View style={styles.searchContainer}>
        <InputAutocomplete
          label="Origin"
          onPlaceSelected={(details) => {
            onPlaceSelected(details, 'origin');
          }}
        />
        <ScrollView>
          {destinations.map((_, index) => (
            <InputAutocomplete
              key={index}
              label={`Destination ${index + 1}`}
              onPlaceSelected={(details) => {
                onPlaceSelected(details, 'destination');
              }}
            />
          ))}
        </ScrollView>
        <TouchableOpacity style={styles.button} onPress={traceRoute}>
          <Text style={styles.buttonText}>Trace route</Text>
        </TouchableOpacity>
        {totalDistance && totalDuration ? (
          <View>
            <Text>Total Distance: {totalDistance.toFixed(2)} Km</Text>
            <Text>Total Duration: {Math.ceil(totalDuration)} min</Text>
          </View>
        ) : null}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  searchContainer: {
    position: 'absolute',
    width: '100%',
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
    padding: 8,
    borderRadius: 8,
    top: Constants.statusBarHeight,
  },
  input: {
    borderColor: '#888',
    borderWidth: 1,
  },
  button: {
    marginTop: 16,
    backgroundColor: '#3498db',
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 8,
    alignSelf: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  edgePadding: {
    top: 100,
    right: 50,
    bottom: 100,
    left: 50,
  },
});
