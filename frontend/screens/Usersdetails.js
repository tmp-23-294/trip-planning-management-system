import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, Image, ActivityIndicator, TouchableOpacity, ScrollView, SafeAreaView } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import { user } from "../assets";
import { Amplify, Storage } from 'aws-amplify';
import awsconfig from '../src/aws-exports';
Amplify.configure(awsconfig);
import NavigationBar from '../components/NavigationBar';

const Usersdetails = () => {
  const navigation = useNavigation();
  const [s3Image, setS3Image] = useState(null);
  const [userData, setUserData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });

    async function getUserData() {
      const username = await AsyncStorage.getItem("username");

      try {
        const userDetailsResponse = await axios.get(
          `http://16.171.19.240:5000/users/${username}`
        );
        setUserData(userDetailsResponse.data.user);
      } catch (error) {
        alert(error.msg);
      }

      try {
        const imageURI = await Storage.get(`profPic/${username}/${username}.jpg`);
        setS3Image(imageURI);
      } catch (error) {
        console.log(error);
      }

      setIsLoading(false);
    }

    getUserData();
  }, []);

  const handleLogout = async () => {
    try {
      // Remove user-related data from local storage
      // await AsyncStorage.removeItem('username'); // Replace 'username' with your storage key
      // await AsyncStorage.removeItem('password'); // Replace 'password' with your storage key
      
      // Navigate to the login or another screen
      navigation.navigate('Login'); // Replace 'LoginScreen' with the screen you want to navigate to
    } catch (error) {
      console.error('Error logging out:', error);
    }
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#333" />
      </View>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
    <View style={styles.container}>
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Image source={user} style={{ width: "120%", height: 800, resizeMode: "cover", position: "absolute", top: 5 }} />
          </View>
      <View style={styles.card}>
        <Text style={styles.heading}>User Details</Text>
        {s3Image && (
          <Image source={{ uri: s3Image }} style={styles.profileImage} />
        )}

        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Name:</Text>
          <Text style={styles.detailText}>
            {userData?.fristname} {userData?.lastname}
          </Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Age:</Text>
          <Text style={styles.detailText}>{userData?.age}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Username:</Text>
          <Text style={styles.detailText}>{userData?.username}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Gender:</Text>
          <Text style={styles.detailText}>{userData?.gender}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Country:</Text>
          <Text style={styles.detailText}>{userData?.country}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Email:</Text>
          <Text style={styles.detailText}>{userData?.email}</Text>
        </View>
        <View style={styles.detailContainer}>
          <Text style={styles.detailLabel}>Telephone:</Text>
          <Text style={styles.detailText}>{userData?.telephone}</Text>
        </View>
        <TouchableOpacity
      style={styles.editButton}
      onPress={() => {
        // Navigate to the Edit Account screen
        navigation.navigate("EditAccount");
      }}
    >
      <Text style={styles.editButtonText}>Edit Account</Text>
    </TouchableOpacity>
    <TouchableOpacity
      style={styles.editButton1}
      onPress={handleLogout}
    >
      <Text style={styles.editButtonText}>LogOut</Text>
    </TouchableOpacity>
      </View>

      <ScrollView  scrollEnabled={false}>
      </ScrollView>
      
      <NavigationBar isProfileDetailsActive={true} />
      
    </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
    backgroundColor: "#e6eeff",
  },
  
  card: {
    backgroundColor: "#fff",
    opacity: 0.9,
    borderRadius: 10,
    padding: 20,
    shadowColor: "#000",
    marginTop:  60,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    width: "90%",
  },
  heading: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#333",
  },
  detailContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
 
  detailLabel: {
    flex: 1,
    fontWeight: "bold",
    color: "#666",
  },
  detailText: {
    flex: 2,
    color: "#333",
  },
  profileImage: {
    width: 100,
    height: 100,
    resizeMode: "cover",
    borderRadius: 50,
    alignSelf: "center",
    marginTop: 0,
    marginBottom: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  editButton: {
    backgroundColor: "#333",
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignSelf: "center",
    marginTop: 20,
  },
  editButton1: {
    backgroundColor: "red",
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignSelf: "center",
    marginTop: 20,
  },
  editButtonText: {
    color: "#fff",
    fontWeight: "bold",
  },
});

export default Usersdetails;
